from setuptools import setup, find_packages
from distutils.command.build import build
import os, glob

LOCALE_DIR= '/usr/share/patibody/languages'

class BuildQm(build):
  os.system('pylupdate5 patibody.pro')
  for ts in glob.glob('languages/*.ts'):
    os.system('lrelease {0} -qm {1}'.format(ts, (ts[:-2]+'qm')))


class BuildRc(build):
  os.system('pyrcc5 patibody.qrc -o patibody/patibody_res.py')

data_files = [("share/applications/", ["files/patibody.desktop"]),
              ]

setup(
    name='patibody',
    version='0.1',
    packages=['patibody'],
    scripts = ['script/patibody'],
    url='',
    license='GPL v3',
    author='Yves Brungard',
    author_email='',
    description='Editor for Patent Applications in XML format',
    #package_data=data_files,
    cmdclass = {'build_qm': BuildQm,
                'build_rc': BuildRc,
                },
)
