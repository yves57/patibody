<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="en">
<context>
    <name>CTextEdit</name>
    <message>
        <location filename="../patibody/patibody.py" line="426"/>
        <source>Chemistry formula %s</source>
        <translation type="unfinished">Formule chimique %s</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="429"/>
        <source>Mathematic formula %s</source>
        <translation>Formule mathématique %s</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="432"/>
        <source>Table %s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="435"/>
        <source>Figure %s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="478"/>
        <source>Add paragraph</source>
        <translation>Ajouter un paragraphe</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="481"/>
        <source>Add title</source>
        <translation>Ajouter un titre</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="484"/>
        <source>Add technical field</source>
        <translation>Ajouter un domaine technique</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="487"/>
        <source>Add prior art</source>
        <translation>Ajouter une technique antérieure</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="490"/>
        <source>Add description of invention</source>
        <translation>Ajouter la description de l&apos;invention</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="493"/>
        <source>Add figures description</source>
        <translation>Ajouter la description des figures</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="496"/>
        <source>Add detailed description</source>
        <translation>Ajouter la description détaillée</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="500"/>
        <source>Add claim</source>
        <translation>Ajouter une revendication</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="505"/>
        <source>Add drawings section</source>
        <translation>Ajouter une section de figures</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="482"/>
        <source>Add table</source>
        <translation type="obsolete">Ajouter un tableau</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="508"/>
        <source>Add image</source>
        <translation>Ajouter une image</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="505"/>
        <source>Invention title</source>
        <translation type="obsolete">Tire de l&apos;invention</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="513"/>
        <source>Technical field</source>
        <translation type="obsolete">Domaine technique</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="521"/>
        <source>Prior art</source>
        <translation type="obsolete">Technique antérieure</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="529"/>
        <source>Disclosure of the invention</source>
        <translation type="obsolete">Exposé de l&apos;invention</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="537"/>
        <source>Brief description of figures</source>
        <translation type="obsolete">Brève description des figures</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="545"/>
        <source>Detailed description</source>
        <translation type="obsolete">Description détaillée</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="553"/>
        <source>Drawings</source>
        <translation type="obsolete">Figures</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="552"/>
        <source>Open image...</source>
        <translation>Ouvrir l&apos;image...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="556"/>
        <source>Images (*.png *.jpg *.bmp);;All Files (*)</source>
        <translation type="obsolete">Images (*.png *.jpg *.bmp);;Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="594"/>
        <source>Claim %d</source>
        <translation>Revendication %d</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="380"/>
        <source>Chemistery formula %s</source>
        <comment>commentaire</comment>
        <translation type="obsolete">Formule chimique</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="552"/>
        <source>Images (*.png *.jpg *.bmp *.tif);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageDialog</name>
    <message>
        <location filename="../patibody/imageInsert.ui" line="14"/>
        <source>Image insertion</source>
        <translation>Insertion d&apos;image</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="51"/>
        <source>Choose image type</source>
        <translation>Choisir un type d&apos;image</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="66"/>
        <source>Figure</source>
        <translation>Figure</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="73"/>
        <source>Table</source>
        <translation>Tableau</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="80"/>
        <source>Mathematic</source>
        <translation>Mathématique</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="87"/>
        <source>Chemistery</source>
        <translation>Chimie</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="107"/>
        <source>Number</source>
        <translation>Numéro</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="121"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="128"/>
        <source>widthField</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="135"/>
        <source>Height</source>
        <translation>Hauteur</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="142"/>
        <source>heightField</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="149"/>
        <source>Width in the &lt;br&gt;document (in mm)</source>
        <translation>Largeur dans le&lt;br&gt;document (en mm)</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="172"/>
        <source>Inline</source>
        <translation>En ligne</translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="94"/>
        <source>Not specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="182"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="250"/>
        <source>%s px</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PatEdit</name>
    <message>
        <location filename="../patibody/patibody.py" line="635"/>
        <source>Left</source>
        <translation type="unfinished">Gauche</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="604"/>
        <source>Right</source>
        <translation type="unfinished">Droite</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="605"/>
        <source>Justify</source>
        <translation type="unfinished">Justifié</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="606"/>
        <source>Center</source>
        <translation type="unfinished">Centré</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="646"/>
        <source>Help</source>
        <translation type="unfinished">Aide</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1989"/>
        <source>About</source>
        <translation type="unfinished">A propos</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="732"/>
        <source>File Actions</source>
        <translation type="unfinished">Actions de fichier</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="735"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="738"/>
        <source>&amp;New</source>
        <translation type="unfinished">&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="746"/>
        <source>&amp;Open...</source>
        <translation type="unfinished">&amp;Ouvrir...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="755"/>
        <source>&amp;Save</source>
        <translation type="unfinished">Enregi&amp;strer</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="763"/>
        <source>Save &amp;As...</source>
        <translation type="unfinished">Enregis&amp;trer sous...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="770"/>
        <source>Document properties</source>
        <translation type="unfinished">Propriétés du document</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="777"/>
        <source>&amp;Print...</source>
        <translation type="unfinished">Im&amp;primer...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="785"/>
        <source>Print Preview...</source>
        <translation type="unfinished">Aperçu avant impression...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="793"/>
        <source>Export P&amp;DF...</source>
        <translation type="unfinished">Exporter  en P&amp;DF</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="803"/>
        <source>&amp;Quit</source>
        <translation type="unfinished">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="809"/>
        <source>Edit Actions</source>
        <translation type="unfinished">Actions d&apos;édition</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="812"/>
        <source>&amp;Edit</source>
        <translation type="unfinished">&amp;Edition</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="815"/>
        <source>&amp;Undo</source>
        <translation type="unfinished">&amp;Défaire</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="822"/>
        <source>&amp;Redo</source>
        <translation type="unfinished">&amp;Rétablir</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="831"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished">Couper</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="838"/>
        <source>&amp;Copy</source>
        <translation type="unfinished">&amp;Copier</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="846"/>
        <source>&amp;Paste</source>
        <translation type="unfinished">Coller</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="856"/>
        <source>Settings</source>
        <translation type="unfinished">Paramètres</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="927"/>
        <source>Format Actions</source>
        <translation type="unfinished">Actions de formatage</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="867"/>
        <source>F&amp;ormat</source>
        <translation type="unfinished">F&amp;ormat</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="870"/>
        <source>&amp;Bold</source>
        <translation type="unfinished">Gras</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="882"/>
        <source>&amp;Superscript</source>
        <translation type="unfinished">Expo&amp;sant</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="891"/>
        <source>&amp;Subscript</source>
        <translation type="unfinished">Indice</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="899"/>
        <source>&amp;Italic</source>
        <translation type="unfinished">&amp;Italique</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="911"/>
        <source>&amp;Underline</source>
        <translation type="unfinished">So&amp;ulignement</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="933"/>
        <source>Standard</source>
        <translation type="unfinished">Standard</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="934"/>
        <source>Bullet List (Disc)</source>
        <translation type="unfinished">Liste à puces (Rond)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="935"/>
        <source>Bullet List (Dash)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="936"/>
        <source>Ordered List (Decimal)</source>
        <translation type="unfinished">Liste numérotée (Chiffre)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="937"/>
        <source>Ordered List (Alpha lower)</source>
        <translation type="unfinished">Liste numérotée (lettre minuscule)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="938"/>
        <source>Ordered List (Alpha upper)</source>
        <translation type="unfinished">Liste numérotée (lettre majuscule)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="939"/>
        <source>Ordered List (Roman lower)</source>
        <translation type="unfinished">Liste numérotée (chiffre romain minusc.)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="940"/>
        <source>Ordered List (Roman upper)</source>
        <translation type="unfinished">Liste numérotée (chiffre romain majusc.)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1334"/>
        <source>Loading</source>
        <translation type="unfinished">Chargement</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1334"/>
        <source>The image %s can&apos;t be found.
</source>
        <translation type="unfinished">L&apos;image %s n&apos;a pas été trouvée.
</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1104"/>
        <source>File not found</source>
        <translation type="unfinished">Fichier non trouvé</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1126"/>
        <source>Document loading</source>
        <translation type="unfinished">Chargement du fichier</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1119"/>
        <source>The document doesn&apos;t pass DTD validation.
                                     Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1126"/>
        <source>The DTD {dtd} is not installed in {path}.
                                     Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1176"/>
        <source>Fig. %s%s</source>
        <translation type="unfinished">Fig. %s%s</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1187"/>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1201"/>
        <source>Titre de l&apos;invention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1208"/>
        <source>Technical field</source>
        <translation type="unfinished">Domaine technique</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1214"/>
        <source>Prior art</source>
        <translation type="unfinished">Technique antérieure</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1220"/>
        <source>Invention description</source>
        <translation type="unfinished">Exposé de l&apos;invention</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1226"/>
        <source>Figures description</source>
        <translation type="unfinished">Description des figures</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1232"/>
        <source>Detailed description</source>
        <translation type="unfinished">Description détaillée</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1246"/>
        <source>Claims</source>
        <translation type="unfinished">Revendications</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1255"/>
        <source>Claim </source>
        <translation type="unfinished">Revendication </translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1281"/>
        <source>Abstract</source>
        <translation type="unfinished">Abrégé</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1317"/>
        <source>Figure %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1347"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation type="unfinished">Le document a été modifié.
Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1368"/>
        <source>%s[*] - %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1431"/>
        <source>Open File...</source>
        <translation type="unfinished">Ouvrir un fichier...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1431"/>
        <source>Application (*.xml *.zip);;All Files (*)</source>
        <translation type="unfinished">Demande (*.xml *.zip);;Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1470"/>
        <source>Unable to save the document</source>
        <translation type="unfinished">Impossible de sauvegarder le document</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1477"/>
        <source>Save as...</source>
        <translation type="unfinished">Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1477"/>
        <source>Zip-Files (*.zip);;All Files (*)</source>
        <translation type="unfinished">Demande (*.zip);;Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1837"/>
        <source>Failed to convert %s to XML</source>
        <translation type="unfinished">Échec de la conversion de %s en XML</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1848"/>
        <source>Print Document</source>
        <translation type="unfinished">Imprimer le document</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1866"/>
        <source>Document PDF generation</source>
        <translation type="unfinished">Générer le document en PDF</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1866"/>
        <source>The document need to be saved.
                                         Do you want to save it?</source>
        <translation type="unfinished">Le document doit être sauvegardé.
Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1872"/>
        <source>Export PDF</source>
        <translation type="unfinished">Exporter en PDF</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1872"/>
        <source>PDF files (*.pdf);;All Files (*)</source>
        <translation type="unfinished">Fichiers PDF (*.pdf);;Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1989"/>
        <source>PatiBody allows to edit text for patent application 
 and save it in XML format.</source>
        <translation type="unfinished">Patibody permet d&apos;éditer des textes de brevet 
et de les enregistrer au format XML.</translation>
    </message>
</context>
<context>
    <name>Properties</name>
    <message>
        <location filename="../patibody/properties.ui" line="14"/>
        <source>Document properties</source>
        <translation>Propriétés du document</translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="47"/>
        <source>Country code to insert in new application body</source>
        <translation>Code de pays à insérer dans le nouveau corps de document</translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="50"/>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="57"/>
        <source>Dtd name</source>
        <translation>Nom de la DTD</translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="64"/>
        <source>Dtd version</source>
        <translation>Version de la DTD</translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="71"/>
        <source>Language of the new document. Used also in the conversion to PDF</source>
        <translation>Langue du nouveau document. Utilisé aussi pour la génération du PDF</translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="74"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../patibody/settings.ui" line="14"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="49"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="52"/>
        <source>General settings</source>
        <translation>Paramètres généraux</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="67"/>
        <source>Fop command</source>
        <translation>Commande fop</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="77"/>
        <source>Fop base path</source>
        <translation>Chemin de base fop</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="87"/>
        <source>Image resolution</source>
        <translation>Résolution de l&apos;image</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="94"/>
        <source>in px per mm</source>
        <translation>en points par mm</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="101"/>
        <source>Document model</source>
        <translation>Modèle de document</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="113"/>
        <source>Editor</source>
        <translation>Éditeur</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="128"/>
        <source>Font name</source>
        <translation>Police</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="138"/>
        <source>Font size</source>
        <translation>Taille de police</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="148"/>
        <source>Indentation</source>
        <translation>Indentation</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="158"/>
        <source>Interline</source>
        <translation>Interligne</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="168"/>
        <source>Alignment</source>
        <translation>Alignement</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="176"/>
        <source>Left</source>
        <translation type="unfinished">Gauche</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="181"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="186"/>
        <source>Center</source>
        <translation>Centré</translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="191"/>
        <source>Justify</source>
        <translation>Justifié</translation>
    </message>
</context>
<context>
    <name>TextEdit</name>
    <message>
        <location filename="../patibody/patibody.py" line="652"/>
        <source>Help</source>
        <translation type="obsolete">Aide</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1972"/>
        <source>About</source>
        <translation type="obsolete">A propos</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="711"/>
        <source>File Actions</source>
        <translation type="obsolete">Actions de fichier</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="714"/>
        <source>&amp;File</source>
        <translation type="obsolete">&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="717"/>
        <source>&amp;New</source>
        <translation type="obsolete">&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="725"/>
        <source>&amp;Open...</source>
        <translation type="obsolete">&amp;Ouvrir...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="734"/>
        <source>&amp;Save</source>
        <translation type="obsolete">Enregi&amp;strer</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="742"/>
        <source>Save &amp;As...</source>
        <translation type="obsolete">Enregis&amp;trer sous...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="749"/>
        <source>Document properties</source>
        <translation type="obsolete">Propriétés du document</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="756"/>
        <source>&amp;Print...</source>
        <translation type="obsolete">Im&amp;primer...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="764"/>
        <source>Print Preview...</source>
        <translation type="obsolete">Aperçu avant impression...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="772"/>
        <source>Export P&amp;DF...</source>
        <translation type="obsolete">Exporter  en P&amp;DF</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="782"/>
        <source>&amp;Quit</source>
        <translation type="obsolete">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="788"/>
        <source>Edit Actions</source>
        <translation type="obsolete">Actions d&apos;édition</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="791"/>
        <source>&amp;Edit</source>
        <translation type="obsolete">&amp;Edition</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="794"/>
        <source>&amp;Undo</source>
        <translation type="obsolete">&amp;Défaire</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="801"/>
        <source>&amp;Redo</source>
        <translation type="obsolete">&amp;Rétablir</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="810"/>
        <source>Cu&amp;t</source>
        <translation type="obsolete">Couper</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="817"/>
        <source>&amp;Copy</source>
        <translation type="obsolete">&amp;Copier</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="825"/>
        <source>&amp;Paste</source>
        <translation type="obsolete">Coller</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="835"/>
        <source>Settings</source>
        <translation type="obsolete">Paramètres</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="906"/>
        <source>Format Actions</source>
        <translation type="obsolete">Actions de formatage</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="846"/>
        <source>F&amp;ormat</source>
        <translation type="obsolete">F&amp;ormat</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="849"/>
        <source>&amp;Bold</source>
        <translation type="obsolete">Gras</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="861"/>
        <source>&amp;Superscript</source>
        <translation type="obsolete">Expo&amp;sant</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="870"/>
        <source>&amp;Subscript</source>
        <translation type="obsolete">Indice</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="878"/>
        <source>&amp;Italic</source>
        <translation type="obsolete">&amp;Italique</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="890"/>
        <source>&amp;Underline</source>
        <translation type="obsolete">So&amp;ulignement</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="912"/>
        <source>Standard</source>
        <translation type="obsolete">Standard</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="913"/>
        <source>Bullet List (Disc)</source>
        <translation type="obsolete">Liste à puces (Rond)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="845"/>
        <source>Bullet List (Circle)</source>
        <translation type="obsolete">Liste à puces (Cercle)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="846"/>
        <source>Bullet List (Square)</source>
        <translation type="obsolete">Liste à puces (Carré)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="915"/>
        <source>Ordered List (Decimal)</source>
        <translation type="obsolete">Liste numérotée (Chiffre)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="916"/>
        <source>Ordered List (Alpha lower)</source>
        <translation type="obsolete">Liste numérotée (lettre minuscule)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="917"/>
        <source>Ordered List (Alpha upper)</source>
        <translation type="obsolete">Liste numérotée (lettre majuscule)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="918"/>
        <source>Ordered List (Roman lower)</source>
        <translation type="obsolete">Liste numérotée (chiffre romain minusc.)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="919"/>
        <source>Ordered List (Roman upper)</source>
        <translation type="obsolete">Liste numérotée (chiffre romain majusc.)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1084"/>
        <source>File not found</source>
        <translation type="obsolete">Fichier non trouvé</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1106"/>
        <source>Document loading</source>
        <translation type="obsolete">Chargement du fichier</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="992"/>
        <source>The document doesn&apos;t pass DTD validation.
                                 Do you want to continue?</source>
        <translation type="obsolete">Le document ne passe pas la validation DTD.
Voulez-vous continuer ?</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1157"/>
        <source>Fig. %s%s</source>
        <translation type="obsolete">Fig. %s%s</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1168"/>
        <source>Description</source>
        <translation type="obsolete">Description</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1189"/>
        <source>Technical field</source>
        <translation type="obsolete">Domaine technique</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1195"/>
        <source>Prior art</source>
        <translation type="obsolete">Technique antérieure</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1201"/>
        <source>Invention description</source>
        <translation type="obsolete">Exposé de l&apos;invention</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1207"/>
        <source>Figures description</source>
        <translation type="obsolete">Description des figures</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1213"/>
        <source>Detailed description</source>
        <translation type="obsolete">Description détaillée</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1227"/>
        <source>Claims</source>
        <translation type="obsolete">Revendications</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1236"/>
        <source>Claim </source>
        <translation type="obsolete">Revendication </translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1262"/>
        <source>Abstract</source>
        <translation type="obsolete">Abrégé</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1280"/>
        <source>Figure </source>
        <translation type="obsolete">Figure </translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1315"/>
        <source>Loading</source>
        <translation type="obsolete">Chargement</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1315"/>
        <source>The image %s can&apos;t be found.
</source>
        <translation type="obsolete">L&apos;image %s n&apos;a pas été trouvée.
</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1328"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation type="obsolete">Le document a été modifié.
Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1349"/>
        <source>%s[*] - %s</source>
        <translation type="obsolete">%s[*] - %s</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1234"/>
        <source>Application body</source>
        <translation type="obsolete">Corps de la demande</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1411"/>
        <source>Open File...</source>
        <translation type="obsolete">Ouvrir un fichier...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1411"/>
        <source>Application (*.xml *.zip);;All Files (*)</source>
        <translation type="obsolete">Demande (*.xml *.zip);;Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1450"/>
        <source>Unable to save the document</source>
        <translation type="obsolete">Impossible de sauvegarder le document</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1455"/>
        <source>Save as...</source>
        <translation type="obsolete">Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1455"/>
        <source>Zip-Files (*.zip);;All Files (*)</source>
        <translation type="obsolete">Demande (*.zip);;Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1819"/>
        <source>Failed to convert %s to XML</source>
        <translation type="obsolete">Échec de la conversion de %s en XML</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1830"/>
        <source>Print Document</source>
        <translation type="obsolete">Imprimer le document</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1848"/>
        <source>Document PDF generation</source>
        <translation type="obsolete">Générer le document en PDF</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1848"/>
        <source>The document need to be saved.
                                         Do you want to save it?</source>
        <translation type="obsolete">Le document doit être sauvegardé.
Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1854"/>
        <source>Export PDF</source>
        <translation type="obsolete">Exporter en PDF</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1854"/>
        <source>PDF files (*.pdf);;All Files (*)</source>
        <translation type="obsolete">Fichiers PDF (*.pdf);;Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1972"/>
        <source>PatiBody allows to edit text for patent application 
 and save it in XML format.</source>
        <translation type="obsolete">Patibody permet d&apos;éditer des textes de brevet 
et de les enregistrer au format XML.</translation>
    </message>
    <message numerus="yes">
        <location filename="../patibody/patibody.py" line="1234"/>
        <source>%s[*] - %s</source>
        <comment>Disambiguation</comment>
        <translation type="obsolete">
            <numerusform>%s[*] - %s</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="641"/>
        <source>Left</source>
        <translation type="obsolete">Gauche</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="610"/>
        <source>Right</source>
        <translation type="obsolete">Droite</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="611"/>
        <source>Justify</source>
        <translation type="obsolete">Justifié</translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="612"/>
        <source>Center</source>
        <translation type="obsolete">Centré</translation>
    </message>
</context>
</TS>
