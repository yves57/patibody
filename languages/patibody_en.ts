<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>CTextEdit</name>
    <message>
        <location filename="../patibody/patibody.py" line="429"/>
        <source>Mathematic formula %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="432"/>
        <source>Table %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="435"/>
        <source>Figure %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="478"/>
        <source>Add paragraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="481"/>
        <source>Add title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="484"/>
        <source>Add technical field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="487"/>
        <source>Add prior art</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="490"/>
        <source>Add description of invention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="493"/>
        <source>Add figures description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="496"/>
        <source>Add detailed description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="500"/>
        <source>Add claim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="505"/>
        <source>Add drawings section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="508"/>
        <source>Add image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="552"/>
        <source>Open image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="594"/>
        <source>Claim %d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="426"/>
        <source>Chemistry formula %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="552"/>
        <source>Images (*.png *.jpg *.bmp *.tif);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageDialog</name>
    <message>
        <location filename="../patibody/imageInsert.ui" line="14"/>
        <source>Image insertion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="51"/>
        <source>Choose image type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="66"/>
        <source>Figure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="73"/>
        <source>Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="80"/>
        <source>Mathematic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="87"/>
        <source>Chemistery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="107"/>
        <source>Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="121"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="128"/>
        <source>widthField</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="135"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="142"/>
        <source>heightField</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="149"/>
        <source>Width in the &lt;br&gt;document (in mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="172"/>
        <source>Inline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="94"/>
        <source>Not specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/imageInsert.ui" line="182"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="250"/>
        <source>%s px</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PatEdit</name>
    <message>
        <location filename="../patibody/patibody.py" line="635"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="604"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="605"/>
        <source>Justify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="606"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="646"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1989"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="732"/>
        <source>File Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="735"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="738"/>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="746"/>
        <source>&amp;Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="755"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="763"/>
        <source>Save &amp;As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="770"/>
        <source>Document properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="777"/>
        <source>&amp;Print...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="785"/>
        <source>Print Preview...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="793"/>
        <source>Export P&amp;DF...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="803"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="809"/>
        <source>Edit Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="812"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="815"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="822"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="831"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="838"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="846"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="856"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="927"/>
        <source>Format Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="867"/>
        <source>F&amp;ormat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="870"/>
        <source>&amp;Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="882"/>
        <source>&amp;Superscript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="891"/>
        <source>&amp;Subscript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="899"/>
        <source>&amp;Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="911"/>
        <source>&amp;Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="933"/>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="934"/>
        <source>Bullet List (Disc)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="935"/>
        <source>Bullet List (Dash)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="936"/>
        <source>Ordered List (Decimal)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="937"/>
        <source>Ordered List (Alpha lower)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="938"/>
        <source>Ordered List (Alpha upper)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="939"/>
        <source>Ordered List (Roman lower)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="940"/>
        <source>Ordered List (Roman upper)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1334"/>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1334"/>
        <source>The image %s can&apos;t be found.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1104"/>
        <source>File not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1126"/>
        <source>Document loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1119"/>
        <source>The document doesn&apos;t pass DTD validation.
                                     Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1126"/>
        <source>The DTD {dtd} is not installed in {path}.
                                     Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1176"/>
        <source>Fig. %s%s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1187"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1201"/>
        <source>Titre de l&apos;invention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1208"/>
        <source>Technical field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1214"/>
        <source>Prior art</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1220"/>
        <source>Invention description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1226"/>
        <source>Figures description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1232"/>
        <source>Detailed description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1246"/>
        <source>Claims</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1255"/>
        <source>Claim </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1281"/>
        <source>Abstract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1317"/>
        <source>Figure %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1347"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1368"/>
        <source>%s[*] - %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1431"/>
        <source>Open File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1431"/>
        <source>Application (*.xml *.zip);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1470"/>
        <source>Unable to save the document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1477"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1477"/>
        <source>Zip-Files (*.zip);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1837"/>
        <source>Failed to convert %s to XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1848"/>
        <source>Print Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1866"/>
        <source>Document PDF generation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1866"/>
        <source>The document need to be saved.
                                         Do you want to save it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1872"/>
        <source>Export PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1872"/>
        <source>PDF files (*.pdf);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/patibody.py" line="1989"/>
        <source>PatiBody allows to edit text for patent application 
 and save it in XML format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Properties</name>
    <message>
        <location filename="../patibody/properties.ui" line="14"/>
        <source>Document properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="47"/>
        <source>Country code to insert in new application body</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="50"/>
        <source>Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="57"/>
        <source>Dtd name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="64"/>
        <source>Dtd version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="71"/>
        <source>Language of the new document. Used also in the conversion to PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/properties.ui" line="74"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../patibody/settings.ui" line="14"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="49"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="52"/>
        <source>General settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="67"/>
        <source>Fop command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="77"/>
        <source>Fop base path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="87"/>
        <source>Image resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="94"/>
        <source>in px per mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="101"/>
        <source>Document model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="113"/>
        <source>Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="128"/>
        <source>Font name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="138"/>
        <source>Font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="148"/>
        <source>Indentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="158"/>
        <source>Interline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="168"/>
        <source>Alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="181"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="186"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="191"/>
        <source>Justify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../patibody/settings.ui" line="176"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
