
QT       += core gui

QT += widgets

TARGET = patibody
TEMPLATE = app


SOURCES += patibody/patibody.py

HEADERS  +=

FORMS    += patibody/settings.ui \
            patibody/imageInsert.ui \
            patibody/properties.ui

TRANSLATIONS     += languages/patibody_en.ts
TRANSLATIONS     += languages/patibody_fr.ts

OTHER_FILES +=
