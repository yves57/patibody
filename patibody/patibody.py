#!/usr/bin/env python3


#############################################################################
##
## Copyright (C) 2013 Riverbank Computing Limited
## Copyright (C) 2010 Hans-Peter Jansen <hpj@urpla.net>.
## Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
##
## This file is part of the examples of PyQt.
##
## $QT_BEGIN_LICENSE:LGPL$
## Commercial Usage
## Licensees holding valid Qt Commercial licenses may use this file in
## accordance with the Qt Commercial License Agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Nokia.
##
## GNU Lesser General Public License Usage
## Alternatively, this file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
##
## In addition, as a special exception, Nokia gives you certain additional
## rights.  These rights are described in the Nokia Qt LGPL Exception
## version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## If you have questions regarding the use of this file, please contact
## Nokia at qt-info@nokia.com.
## $QT_END_LICENSE$
##
#############################################################################

APP = "Patibody"
VERS = "0.1"

import sys
import re
import io
import os
import zipfile
from PyQt5.QtCore import QFile, QFileInfo, Qt,\
    QUrl, QTextStream, QByteArray, QAbstractItemModel, QModelIndex, \
    QIODevice, QTemporaryDir, QLocale, QTranslator, QLibraryInfo, QSettings,\
    QCoreApplication, QSize, QBuffer
from PyQt5.QtXml import QDomDocument, QDomImplementation
from PyQt5.QtGui import (QFont, QFontDatabase, QIcon, QKeySequence,
        QTextBlockFormat, QTextCharFormat, QTextCursor,
        QTextListFormat, QTextBlockUserData,
        QTextDocumentFragment, QImage, QTextDocument,
        QPixmap, QTextImageFormat)
from PyQt5.QtWidgets import (QAction, QApplication,
        QComboBox, QFileDialog, QDialog, QMainWindow, QMenu, QMessageBox,
        QTextEdit, QToolBar, QDialogButtonBox, QGroupBox, QVBoxLayout,
        QAbstractScrollArea, QSizePolicy, QScrollArea, QWidget, QTabWidget,
        QLabel)

#  Used for managing TIFF images
from PIL import Image, ImageQt

try:
    import patibody.patibody_res
    from patibody.imageinsert import Ui_ImageDialog
    from patibody.settings import Ui_Settings
    from patibody.properties import Ui_Properties
except:
    import patibody_res
    from imageinsert import Ui_ImageDialog
    from settings import Ui_Settings
    from properties import Ui_Properties

if sys.platform.startswith('darwin'):
    rsrcPath = ":/images/mac"
else:
    rsrcPath = ":/images/win"

class DomItem(object):
    def __init__(self, node, row, parent=None):
        self.domNode = node
        # Record the item's location within its parent.
        self.rowNumber = row
        self.parentItem = parent
        self.childItems = {}

    def node(self):
        return self.domNode

    def parent(self):
        return self.parentItem

    def child(self, i):
        if i in self.childItems:
            return self.childItems[i]

        if i >= 0 and i < self.domNode.childNodes().count():
            childNode = self.domNode.childNodes().item(i)
            childItem = DomItem(childNode, i, self)
            self.childItems[i] = childItem
            return childItem

        return None

    def row(self):
        return self.rowNumber


class DomModel(QAbstractItemModel):
    def __init__(self, document, parent=None):
        super(DomModel, self).__init__(parent)

        self.domDocument = document

        self.rootItem = DomItem(self.domDocument, 0)

    def columnCount(self, parent):
        return 3

    def data(self, index, role):
        if not index.isValid():
            return None

        if role != Qt.DisplayRole:
            return None

        item = index.internalPointer()

        node = item.node()
        attributes = []
        attributeMap = node.attributes()

        if index.column() == 0:
            return node.nodeName()

        elif index.column() == 1:
            for i in range(0, attributeMap.count()):
                attribute = attributeMap.item(i)
                attributes.append(attribute.nodeName() + '="' +
                                  attribute.nodeValue() + '"')

            return " ".join(attributes)

        if index.column() == 2:
            value = node.nodeValue()
            if value is None:
                return ''

            return ' '.join(node.nodeValue().split('\n'))

        return None

    def flags(self, index):
        if not index.isValid():
            return Qt.NoItemFlags

        return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            if section == 0:
                return "Name"

            if section == 1:
                return "Attributes"

            if section == 2:
                return "Value"

        return None

    def index(self, row, column, parent):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QModelIndex()

    def parent(self, child):
        if not child.isValid():
            return QModelIndex()

        childItem = child.internalPointer()
        parentItem = childItem.parent()

        if not parentItem or parentItem == self.rootItem:
            return QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent):
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        return parentItem.node().childNodes().count()

class ImageDialog(QDialog):
    def __init__(self):
        super(ImageDialog, self).__init__()
        self.setModal(True)
        # Set up the user interface from Designer.
        self.ui = Ui_ImageDialog()
        self.ui.setupUi(self)

    def setParams(self, tab, wi, he, wiInDoc, name):
        if tab == 3:   # drawings tab
            self.ui.figureRadio.setChecked(True)
            self.ui.tableRadio.setEnabled(False)
            self.ui.chemRadio.setEnabled(False)
            self.ui.mathRadio.setEnabled(False)
            self.ui.otherButton.setEnabled(False)
        else:
            self.ui.otherButton.setChecked(True)
            self.ui.figureRadio.setEnabled(False)
        self.ui.nameEdit.setText(name)
        self.ui.widthField.setText(self.tr("%s px") % wi)
        self.ui.heightField.setText(self.tr("%s px") % he)
        self.ui.widthInDocument.setText("%d"%wiInDoc)

class SettingsDialog(QDialog):
    def __init__(self):
        super(SettingsDialog, self).__init__()
        self.setModal(True)
        # Set up the user interface from Designer.
        self.ui = Ui_Settings()
        self.ui.setupUi(self)
        db = QFontDatabase()
        for size in db.standardSizes():
            self.ui.fontSizeComboBox.addItem("%s" % (size))

class PropertiesDialog(QDialog):
    def __init__(self):
        super(PropertiesDialog, self).__init__()
        self.setModal(True)
        # Set up the user interface from Designer.
        self.ui = Ui_Properties()
        self.ui.setupUi(self)

class MetaData(QTextBlockUserData):
    def __init__(self, tag = "",id="", lang="", status="", country="", he="", wi="", orientation="", img_format=""):
        super(MetaData, self).__init__()
        self.tag = tag
        self.id = id
        self.lang = lang
        self.status = status
        self.country = country
        self.he = he
        self.wi = wi
        self.orientation = orientation
        self.img_format = img_format

    def id(self):
        return self.id

    def orientation(self):
        return self.orientation

    def wi(self):
        return self.wi

    def he(self):
        return self.he

    def img_format(self):
        return self.img_format

class ImagesData():
    def __init__(self):
        self.reset()

    def addImageData(self, name, num, height, width, orientation, img_format, category):
        self._num[name] = num
        self._height[name] = height
        self._width[name] = width
        self._orientation[name] = orientation
        self._img_format[name] = img_format
        self._category[name] = category
        # TODO Faut-il stocker aussi les données de l'image ?

    def num(self, name):
        return self._num[name]

    def height(self, name):
        return self._height[name]

    def width(self, name):
        return self._width[name]

    def orientation(self, name):
        return self._orientation[name]

    def img_format(self, name):
        return self._img_format[name]

    def category(self, name):
        return self._category[name]

    def names(self):
        names = []
        for path in self._category.keys():
            names.append(os.path.basename(path))
        return names

    def reset(self):
        self._num = {}
        self._height = {}
        self._width = {}
        self._img_format = {}
        self._orientation = {}
        self._category = {}

class Figure(QGroupBox):
    def __init(self, text):
        super(ChapterEdit, self).__init__()
        self.img_id = ""

    def setId(self, img_id):
        self.img_id = img_id

class PasteImage():
    def __init(self, text):
        self.setAcceptDrops(True)
        print(text)
        self.setText(text)
        self.setReadOnly(True)
        self.setEnabled(False)
        self.pasting = False

    def dragEnterEvent(self, evt):
        if (evt.mimeData().hasFormat("text/plain")):
            evt.acceptProposedAction()
        print("Débute glisser")

    def eventDrop(self, evt):
        '''
        Insert the pasted data.
        For image, open a dialog to get information about it.
        The identifier is used to name the image which will be saved under.
        '''
        # widget = self.focusWidget()
        source = evt.mimeData()
        tab_index = patEdit.tabs.currentIndex()
        print(tab_index)
        if tab_index == 3:
            self.source = source
            if source.hasImage():
                self.image = QImage(self.source.imageData())
                self.imageDialog = ImageDialog()
                self.imageDialog.setParams(tab_index, self.image.width(), self.image.height(),
                                           self.image.width()/patEdit.resolution, "")
                self.imageDialog.ui.buttonBox.accepted.connect(self.accept)
                self.imageDialog.ui.buttonBox.rejected.connect(self.reject)
                self.imageDialog.exec_()

    def insertFromMimeData(self, source ):
        '''
        Insert the pasted data.
        For image, open a dialog to get information about it.
        The identifier is used to name the image which will be saved under.
        '''
        self.source = source
        if patEdit.tabs.currentIndex() != 3:
            self.insertionCursor = self.textCursor()
        if source.hasImage():
            self.pasting = True
            #TODO Test if image is authorized. Not for invention-title section
            image = QImage(self.source.imageData()).convertToFormat(QImage.Format_Mono)
            buffer = QBuffer()
            buffer.open(QBuffer.ReadWrite)
            image.save(buffer, "TIFF")
            self.PILimage = Image.open(io.BytesIO(buffer.data()))
            self.imageDialog = ImageDialog()
            tab_index = patEdit.tabs.currentIndex()
            self.imageDialog.setParams(tab_index, self.PILimage.width, self.PILimage.height,
                                       self.PILimage.width/patEdit.resolution, "")
            self.imageDialog.ui.buttonBox.accepted.connect(self.accept)
            self.imageDialog.ui.buttonBox.rejected.connect(self.reject)
            self.imageDialog.ui.inlineCheckBox.setEnabled(True)
            self.imageDialog.exec_()
        elif source.hasText() and patEdit.tabs.currentIndex() != 3:
                # Paste other data as text. HTML format could insert non wanted stuff.
                # Text is ignored in Drawings chapter
                self.textCursor().insertText(source.text())

    def accept(self):
        '''
        Execute the image insertion at the current position after the imageDialog
        is closed and with the given settings.
        The image is saved in TIFF format in the temp directory with compression 'group4', required by EPO.
        '''
        img_id = self.imageDialog.ui.nameEdit.text()+".tif"
        path = os.path.join(patEdit.tempdir(), img_id )
        imagefmt = QTextImageFormat()
        imagefmt.setName(path)
        wi = float(self.imageDialog.ui.widthInDocument.text())
        try:
            wi = float(self.imageDialog.ui.widthInDocument.text())
            wi = wi if wi > 0 else (self.PILimage.width / patEdit.resolution)
            imagefmt.setWidth(wi * patEdit.resolution)
            he = wi * self.PILimage.height/self.PILimage.width
            orientation = "portrait" if wi < he else "landscape"
        except:
            pass

        tab_index = patEdit.tabs.currentIndex()
        if tab_index == 3:
#            Image from clipboard
            if self.pasting:
                self.pasteDrawing(img_id, num=int(self.imageDialog.ui.numEdit.text()) )
                self.pasting = False
            else:
                # Insertion from a file
                pixmap = QPixmap().fromImage(ImageQt.toqimage(self.PILimage.resize((int(wi * patEdit.resolution), int(he * patEdit.resolution)))))
                self.addDrawing(self.tr('Figure {}').format( self.imageDialog.ui.numEdit.text()), pixmap, img_id)
            patEdit.image_data.addImageData(img_id, self.imageDialog.ui.numEdit.text(), he, wi, orientation, "tif" ,'figure')
            self.drawings_modified = True
            patEdit.actionSave.setEnabled(True)
            patEdit.setWindowModified(True)
        else:
            document = self.document()
            document.addResource(QTextDocument.ImageResource,
                                 QUrl(path), ImageQt.toqimage(self.PILimage))
            cursor = self.insertionCursor
            if self.imageDialog.ui.chemRadio.isChecked():
                category = "chemistry"
                label = self.tr("Chemistry formula %s")
            elif self.imageDialog.ui.mathRadio.isChecked():
                category = "maths"
                label = self.tr("Mathematic formula %s")
            elif self.imageDialog.ui.tableRadio.isChecked():
                category = "tables"
                label = self.tr("Table %s")
            elif self.imageDialog.ui.figureRadio.isChecked():
                category = "figure"
                label = self.tr("Figure %s")
            else:
                category = ""
                label = ""
            if self.imageDialog.ui.inlineCheckBox.isChecked():
                cursor.insertImage(imagefmt)
            else:
                cursor.insertBlock()
                if self.imageDialog.ui.figureRadio.isChecked():
                    cursor.insertFrame(self.defaultFrameFormat)
                if label != "":
                    cursor.insertText(label%self.imageDialog.ui.numEdit.text())
                    cursor.insertBlock()
                cursor.insertImage(imagefmt)
            patEdit.image_data.addImageData(path, self.imageDialog.ui.numEdit.text(), he, wi, orientation, "tif" ,category)
        #  Write image in temp directory
        self.PILimage.save(path, format='tiff', compression='group4', dpi=(300,300), quality=80)

    def reject(self):
        self.image = None

    def contextMenuEvent(self, event):
        tab_index = patEdit.tabs.currentIndex()
        print(f"Context menu {tab_index}")
        if tab_index == 0:  #        description
            menu = self.createStandardContextMenu()
            self.insertionCursor = self.cursorForPosition(event.pos())
            actionPara = QAction(self.tr("Add paragraph"), self,
                           triggered = self.addPara)
            menu.addAction(actionPara)
            actionTitle = QAction(self.tr("Add title"), self,
                           triggered = self.addTitle)
            menu.addAction(actionTitle)
            actionField = QAction(self.tr("Add technical field"), self,
                           triggered = self.addField)
            menu.addAction(actionField)
            actionPriorart = QAction(self.tr("Add prior art"), self,
                           triggered = self.addPriorart)
            menu.addAction(actionPriorart)
            actionInvention = QAction(self.tr("Add description of invention"), self,
                           triggered = self.addInvention)
            menu.addAction(actionInvention)
            actionFigureDesc = QAction(self.tr("Add figures description"), self,
                           triggered = self.addFigureDesc)
            menu.addAction(actionFigureDesc)
            actionEmbodiment = QAction(self.tr("Add detailed description"), self,
                           triggered = self.addEmbodiment)
            menu.addAction(actionEmbodiment)
            actionDelete = QAction(self.tr("Remove this section"), self,
                           triggered = self.deleteSection)
            menu.addAction(actionDelete)
        elif tab_index == 1:      #  claims
            menu = self.createStandardContextMenu()
            actionClaim = QAction(self.tr("Add claim"), self,
                           triggered = patEdit.tabs.widget(tab_index).widget().insertClaim)
            menu.addAction(actionClaim)
            actionDelClaim = QAction(self.tr("Delete claim"), self,
                           triggered = patEdit.tabs.widget(tab_index).widget().deleteClaim)
            menu.addAction(actionDelClaim)
        elif tab_index == 3:
            #  TODO Manage insertion/deletion of drawings tab
#            if not ('drawings' in self.sections.keys()):
#                actionDrawings = QAction(self.tr("Add drawings section"), self,
#                                      triggered=self.addDrawings)
#                menu.addAction(actionDrawings)
            menu = QMenu()
            actionImage = QAction(self.tr("Add image"), self,
                           triggered = self.addImage)
            menu.addAction(actionImage)
        menu.exec_(self.mapToGlobal(event.pos()))
        del menu

    def addImage(self):
        fn, _ = QFileDialog.getOpenFileName(self, self.tr("Open image..."), None,
                                            self.tr("Images (*.png *.jpg *.bmp *.tif);;All Files (*)"))

        self.PILimage = Image.open(fn).convert(mode="1")
        self.imageDialog = ImageDialog()
        # no pasting operation, but insertion from file
        self.pasting = False

        tab_index = patEdit.tabs.currentIndex()
        self.imageDialog.ui.buttonBox.accepted.connect(self.accept)
        self.imageDialog.ui.buttonBox.rejected.connect(self.reject)
        name = os.path.basename(fn)
        if not (name in patEdit.image_data.names()):
            name, ext = os.path.splitext(name)
        self.imageDialog.setParams(tab_index, self.PILimage.width, self.PILimage.height,
                                   self.PILimage.width / patEdit.resolution, name)
        if tab_index == 3 :   # drawings
             self.imageDialog.ui.figureRadio.setChecked(True)
             self.imageDialog.ui.tableRadio.setEnabled(False)
             self.imageDialog.ui.chemRadio.setEnabled(False)
             self.imageDialog.ui.mathRadio.setEnabled(False)
             self.imageDialog.ui.otherButton.setEnabled(False)
        else:
             self.imageDialog.ui.otherButton.setChecked(True)
             self.imageDialog.ui.figureRadio.setEnabled(False)
        self.imageDialog.exec_()

class ChapterEdit(QWidget, PasteImage):
    def __init__(self, tag):
        super(ChapterEdit, self).__init__()

        # Collections of elements which contain the elements of document
        self.lang = ""
        self.tag = tag
        self.vb = QVBoxLayout()
        self.setLayout(self.vb)
        # flag for drawings chapter. Other parts are managed as QTextDocument
        self.drawings_modified = False

#        self.defaultFrameFormat = None
#        self.resolution = 3.0
        self.tags_dict = {"invention-title": self.tr("Invention title"),
                          "technical-field": self.tr("Technical field"),
                          "background-art": self.tr("Prior art"),
                          "disclosure": self.tr("Disclosure of the invention"),
                          "description-of-drawings": self.tr("Brief description of figures"),
                          "mode-for-invention": self.tr("Detailed description"),
                          "abstract": self.tr("Abstract"),
                          "drawings": self.tr("Drawings"),
                          }

    '''
    def contextMenuEvent(self, event):
        #menu = self.createStandardContextMenu()
        menu = QMenu()
        tab_index = patEdit.tabs.currentIndex()
        print(f"Chapter edit context menu {tab_index}")
        if tab_index == 3:
            #  TODO Manage insertion/deletion of drawings tab
#            if not ('drawings' in self.sections.keys()):
#                actionDrawings = QAction(self.tr("Add drawings section"), self,
#                                      triggered=self.addDrawings)
#                menu.addAction(actionDrawings)
            print(patEdit.tabs.widget(tab_index).widget())
            actionImage = QAction(self.tr("Add image"), self,
                           triggered = self.addFirstFigure)
            menu.addAction(actionImage)
        menu.exec_(self.mapToGlobal(event.pos()))
        del menu
    '''

    def editResize(self, section):
        '''
        Adapt the size of the container to its content
        '''
        section.docHeight = section.document().size().height()
        section.updateGeometry()

    def changed(self, section):
        '''
        Occur when the content of the container is changed
        Set the document as modified to allow to save it
        Adapt the size of the container to its content
        '''
        self.editor.setWindowModified(True)
        self.editor.actionSave.setEnabled(True)
        self.editResize(section)

    def insertClaim(self):
        '''
        Insert a container for a new claim after the current claim
        and renumber the remaining claims
        '''
        widget = self.focusWidget()
        if widget.metaObject().className() == "CTextEdit":
            if widget.number:
                self.addClaim(widget.number + 1, position=widget.number)
                for i in range(len(self.vb) - 1, widget.number, -1 ):
                    self.vb.itemAt(i).widget().setTitle(f"Claim {i+1}")
                    self.vb.itemAt(i).widget().layout().itemAt(0).widget().number = i + 1

    def deleteClaim(self):
        '''
        Remove the object containing the claim where the cursor is
        and renumber the remaining claims
        '''
        widget = self.focusWidget()
        if widget.metaObject().className() == "CTextEdit":
            if widget.number:
                current_index = widget.number - 1
                for i in range(len(self.vb) - 1 , current_index, -1 ):
                    self.vb.itemAt(i).widget().setTitle(f"Claim {i}")
                    self.vb.itemAt(i).widget().layout().itemAt(0).widget().number = i
                self.vb.itemAt(current_index).widget().deleteLater()

    def addClaim(self, num, position=-1):
        '''
        Insert a container for a new claim
        num: the number of the new claim
        position: -1 after the last claim, or at the specified "position"
        '''
        tag = f"Claim {num}"
        cursor, te = self.addSection(tag, position)
        te.number = int(num)
        return cursor, te

    def addDrawing(self, tag, image, img_id, position=-1):
        # TODO Créer une classe commune aux sections pour gérer le menu contextuel (ou pas)
        child = self.vb.itemAt(0)
        if child and (child.widget().img_id == "dummy_id"):
            child.widget().setId(img_id)
            child.widget().setTitle(tag)
            child.widget().layout().itemAt(0).widget().setPixmap(image)
        else:
            gb = Figure(tag)
            gb.setId(img_id)
            label = QLabel()
            label.setPixmap(image)
            vb1 = QVBoxLayout()
            vb1.addWidget(label )
            gb.setLayout(vb1)
            self.vb.insertWidget(position,gb)

    def pasteDrawing(self, img_id, num=-1):
        '''
        Add a figure with data from the clipboard
        '''
        md = QApplication.clipboard().mimeData()
        if md.hasImage():
            pixmap = QPixmap()
            pixmap.convertFromImage(md.imageData().convertToFormat(QImage.Format_Mono))
            if num == -1:
                num = self.vb.count() + 1
            self.addDrawing(self.tr("Figure %d")% num, pixmap, img_id)

    def insertSection(self, section, tag):
        '''
        Add a new section after the section passed as argument

        :param section: the section
        :type section: CTextEdit object
        :param tag: the name for the new section
        :type tag: string
        :return cursor: a cursor pointing at the begin of the new section
        :rtype QCursor
        '''
        p = self.vb.indexOf(section.parent()) + 1
        cursor, te = self.addSection(tag, position=p)
        return cursor

    def addSection(self, tag, position=-1):
        '''
        Add a new section. The section added is a CTextEdit class.
        :param tag: the title of the section
        :param position: -1, after the last section, or at the specified "position"

        :returns:
            :return cursor: a cursor pointing at the begin of the new section
            :rtype cursor:  QCursor
            :return te: the new section
            :rtype te: CTextEdit
        '''
        self.editor = self.parent().parent().parent().parent().parent()
        # get patEdit
        if tag in self.tags_dict.keys():
            gb = QGroupBox(self.tags_dict[tag])
        else:
            gb = QGroupBox(tag)
        te = CTextEdit(tag)
        vb1 = QVBoxLayout()
        vb1.addWidget(te )
        self.vb.insertWidget(position,gb)
        gb.setLayout(vb1)
        te.document().contentsChanged.connect(lambda: self.changed(te))
        te.document().undoAvailable.connect(self.editor.actionUndo.setEnabled)
        te.document().redoAvailable.connect(self.editor.actionRedo.setEnabled)
        te.currentCharFormatChanged.connect(lambda format: self.currentCharFormatChanged(format, te))
        te.cursorPositionChanged.connect(lambda: self.editor.cursorPositionChanged(te))
        cursor = QTextCursor(te.document())
        cursor.atStart()
        return cursor, te

    def remove(self, section):
        '''
        Remove the section passed as argument
        '''
        index = self.vb.indexOf(section.parent())
        child = self.vb.takeAt(index)
        if child.widget():
            child.widget().deleteLater()

    def clear(self):
        '''
        Delete all the content of the chapter
        '''
        while len(self.vb):
            child = self.vb.takeAt(0)
            if child.widget():
                child.widget().deleteLater()

    def doc(self, index):
        '''
        Return the document of the section in the chapter designated by the index argument
        '''
        return self.vb.itemAt(index).widget().layout().itemAt(0).widget().document()

    def currentCharFormatChanged(self, format, te):
        self.editor.fontChanged(format.font())
        self.editor.actionTextSuperScript.setChecked(format.verticalAlignment() == QTextCharFormat.AlignSuperScript)
        self.editor.actionTextSubScript.setChecked(format.verticalAlignment() == QTextCharFormat.AlignSubScript)

class CTextEdit(QTextEdit, PasteImage):
    '''
    Content a section of the document. This section contains formatted text and images.
    It is included in a GroupBox which has a title according to the tag of the section
    '''
    def __init__(self, tag):
        super(CTextEdit, self).__init__()

#        self.image_data = ImagesData()
        self.docHeight = 100
        self.tag = tag

        #  used for claims
        self.number = 0

        self.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)
        self.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContentsOnFirstShow)

    def sizeHint(self):
        s = QSize(self.document().size().toSize())
        s.setHeight(self.docHeight + self.contentsMargins().top() + self.contentsMargins().bottom())
        return s

    def setFormat(self):
        cursor = QTextCursor(self.document())
        cursor.movePosition(QTextCursor.Start)
        cursor.movePosition(QTextCursor.End, QTextCursor.KeepAnchor)

        patEdit.charfmt.setFontFamily(patEdit.fontName)
        patEdit.charfmt.setFontPointSize(patEdit.fontSize)
        patEdit.blockfmt.setAlignment(patEdit.qtalign[patEdit.alignment])
        patEdit.blockfmt.setTextIndent(patEdit.indentation)
        patEdit.blockfmt.setLineHeight(patEdit.interline, 4)  # 4: LineDistanceHeight
        cursor.mergeCharFormat(patEdit.charfmt)
        cursor.mergeBlockFormat(patEdit.blockfmt)


#    def addPara(self):
#        cursor = self.insertionCursor
#        cursor.movePosition(QTextCursor.EndOfBlock)
#        cursor.insertBlock()

    def addTitle(self):
        tab_index = patEdit.tabs.currentIndex()
        patEdit.tabs.widget(tab_index).widget().insertSection( self,  tag="invention-title")

    def addField(self):
        tab_index = patEdit.tabs.currentIndex()
        patEdit.tabs.widget(tab_index).widget().insertSection( self,  tag="technical-field")

    def addPriorart(self):
        tab_index = patEdit.tabs.currentIndex()
        patEdit.tabs.widget(tab_index).widget().insertSection( self, tag="background-art")

    def addInvention(self):
        tab_index = patEdit.tabs.currentIndex()
        patEdit.tabs.widget(tab_index).widget().insertSection( self,  tag="disclosure")

    def addFigureDesc(self):
        tab_index = patEdit.tabs.currentIndex()
        patEdit.tabs.widget(tab_index).widget().insertSection( self, tag="description-of-drawings")

    def addEmbodiment(self):
        tab_index = patEdit.tabs.currentIndex()
        patEdit.tabs.widget(tab_index).widget().insertSection( self,  tag="mode-for-invention")

    def deleteSection(self):
        tab_index = patEdit.tabs.currentIndex()
        patEdit.tabs.widget(tab_index).widget().remove(self)
#
#    def addDrawings(self):
#        self.addSection(self.insertionCursor, tag="drawings")
#
#    def idFrame(self,frame):
#        """ Return the key of item frame in sections
#         Return None of not found """
#        for key in self.sections.keys():
#            if frame == self.sections[key]:
#                return key
#        return None
#
#    def contentFrame(self, cursor):
#        """ Return the key of item frame where the cursor is """
#        cursor.movePosition(QTextCursor.PreviousBlock)
#        frame = cursor.currentFrame()
#        return(self.idFrame(frame))

    def addPara(self):
        cursor = self.insertionCursor
        cursor.movePosition(QTextCursor.EndOfBlock)
        cursor.insertBlock()
#
#    def getnum(self,newframe):
#        listframes = self.claimFrame.childFrames()
#        num = 1
#        for frame in listframes :
#            if frame == newframe :
#                retnum = num
#            cursor = frame.firstCursorPosition()
#            cursor.select(QTextCursor.LineUnderCursor)
#            cursor.removeSelectedText()
#            cursor.insertText(self.tr("Claim %d")%num)
#            cursor.block().setUserData(MetaData(tag="claim", id='%d'%num))
#            num +=1
#        return retnum

class ChapterTab(QScrollArea):
    def __init__(self, tag, parent=None):
        super(ChapterTab, self).__init__(parent)
        self.edit = ChapterEdit(tag)
        self.setWidget(self.edit)
        self.setWidgetResizable(True)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

class PatEdit(QMainWindow):
    def __init__(self, fileName=None, parent=None):
        super(PatEdit, self).__init__(parent)

        #  TODO : se passer de la traduction, susceptible de générer des erreurs si la langue change.
        self.qtalign = {self.tr("Left"): Qt.AlignLeft,
                   self.tr("Right"): Qt.AlignRight,
                   self.tr("Justify"): Qt.AlignJustify,
                   self.tr("Center"): Qt.AlignCenter}
        self.currentListFormat = None

        self.styleDict = {
            1: QTextListFormat.ListDisc,
            2: QTextListFormat.ListSquare,
            3: QTextListFormat.ListDecimal,
            4: QTextListFormat.ListLowerAlpha,
            5: QTextListFormat.ListUpperAlpha,
            6: QTextListFormat.ListLowerRoman,
            7: QTextListFormat.ListUpperRoman,
        }

        self.listTypeDict = {
            QTextListFormat.ListDisc: "bullet",
            QTextListFormat.ListSquare: "dash",
            QTextListFormat.ListDecimal: "1",
            QTextListFormat.ListLowerAlpha: "a",
            QTextListFormat.ListUpperAlpha: "A",
            QTextListFormat.ListLowerRoman: "i",
            QTextListFormat.ListUpperRoman: "I",
        }
        #   settings
        self.mysettings = QSettings("Patibody", "Patibody")

        self.fop_path = self.mysettings.value("General/foppath",QCoreApplication.applicationDirPath())
        self.fop_cmd = self.mysettings.value("General/fopcommand","bash fop1.sh")
        # scale for displaying images, in px/mm. 2.9 is 72 dpi.
        self.resolution = float(self.mysettings.value("Display/resolution","2.9"))
        self.alignment = self.mysettings.value("Display/alignment",self.tr("Left"))
        self.fontName = self.mysettings.value("Display/family","DejaVu Sans")
        self.fontSize = float(self.mysettings.value("Display/size", 14))
        self.indentation = float(self.mysettings.value("Display/indentation", 0))
        self.interline = float(self.mysettings.value("Display/interline", 1))
        self.newDocument = self.mysettings.value("General/docmodel")

        self.setWindowIcon(QIcon(':/images/logo.png'))
        self.setToolButtonStyle(Qt.ToolButtonFollowStyle)
        self.setupFileActions()
        self.setupEditActions()
        self.setupTextActions()

        helpMenu = QMenu(self.tr("Help"), self)
        self.menuBar().addMenu(helpMenu)
        helpMenu.addAction(self.tr("About"), self.about)

        # Create Tabs
        self.tabs = QTabWidget()
        self.setCentralWidget(self.tabs)

        self.desc_tab = ChapterTab('description')
        self.claims_tab = ChapterTab('claims')
        self.abstract_tab = ChapterTab('abstract')
        self.drawings_tab = ChapterTab('drawings')
        self.desc_edit = self.desc_tab.edit
#        self.desc_edit.resolution = resolution
        self.tabs.currentChanged.connect(self.tabChanged)
        self.tabs.addTab(self.desc_tab, self.tr("Description"))
        self.claims_edit = self.claims_tab.edit
        self.tabs.addTab(self.claims_tab, self.tr("Revendications"))
        self.abstract_edit = self.abstract_tab.edit
        self.tabs.addTab(self.abstract_tab, self.tr("Abstract"))
        self.drawings_edit = self.drawings_tab.edit
        self.tabs.addTab(self.drawings_tab, self.tr("Drawings"))
        self.currentEdit = self.desc_edit
        #self.desc_edit1.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)
        self.desc_tab.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        self.desc_tab.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContentsOnFirstShow)
        self.desc_tab.setFocus()

        self.image_data = ImagesData()
        self.setCurrentFileName()
        self.actionUndo.setEnabled(False)
        self.actionRedo.setEnabled(False)
        self.actionUndo.triggered.connect(self.undo)
        self.actionRedo.triggered.connect(self.redo)
        self.actionCut.triggered.connect(self.cut)
        self.actionCopy.triggered.connect(self.copy)
        self.actionPaste.triggered.connect(self.paste)
        self.actionSettings.triggered.connect(self.settings)
        self.actionProperties.triggered.connect(self.properties)
        QApplication.clipboard().dataChanged.connect(self.clipboardDataChanged)
        #QApplication.focusChanged.connect(self.editFocusChanged)

        if fileName is None:
            fileName = self.newDocument

        if not self.load(fileName):
            self.fileNew()
        self.setWindowModified(False)
        self.actionSave.setEnabled(False)

    def undo(self):
        #   send the command to thee active section
        #   TODO  ce devrait être la section où vers la dernière action a été faite
        widget = self.focusWidget()
        if widget.metaObject().className() == "CTextEdit":
            widget.undo()

    def cut(self):
        #   send the command to the active section
        widget = self.focusWidget()
        if widget.metaObject().className() == "CTextEdit":
            widget.cut()

    def copy(self):
        #   send the command to the active section
        widget = self.focusWidget()
        if widget.metaObject().className() == "CTextEdit":
            widget.copy()

    def paste(self):
        #   send the command to the active section
        widget = self.focusWidget()
        print(widget)
        if widget.metaObject().className() == "CTextEdit":
            widget.paste()
        if widget.metaObject().className() == "ChapterTab":
            widget.edit.insertFromMimeData(QApplication.clipboard().mimeData())

    def redo(self):
        #   send the command to thee active section
        widget = self.focusWidget()
        if widget.metaObject().className() == "CTextEdit":
            widget.redo()

    def editFocusChanged(self, old_widget, new_widget):
        if new_widget:
            if new_widget.metaObject().className() == "CTextEdit":
                self.cursorPositionChanged(new_widget)

    def docResize(self):
        for tab in (self.desc_edit, self.claims_edit, self.abstract_edit):
            for i in range(0, len(tab.vb)):
                section = tab.vb.itemAt(i).widget().layout().itemAt(0).widget()
                tab.editResize(section)

    def tabChanged(self, tab_index):
    #   don't apply to drawings tab (3)
        if tab_index != 3:
            edit = self.tabs.widget(tab_index).widget()
            edit_layout = self.tabs.widget(tab_index).widget().layout()
            for i in range(0, edit_layout.count()):
                if edit_layout.itemAt(i).widget():
                    edit.editResize(edit_layout.itemAt(i).widget().layout().itemAt(0).widget())

    def setModified(self, up):
        if not up:
            for chapter in (self.desc_edit, self.claims_edit, self.abstract_edit):
                for i in range(0, chapter.vb.count()):
                    chapter.doc(i).setModified(False)
            self.drawings_edit.drawings_modified = False
            self.setWindowModified(False)

    def isModified(self):
        isModified = False
        for chapter in (self.desc_edit, self.claims_edit, self.abstract_edit):
            for i in range(0, chapter.vb.count()):
                isModified = isModified or chapter.doc(i).isModified()
        isModified = isModified or self.drawings_edit.drawings_modified

    def tempdir(self):
        try:
            a = self._tempdir
        except AttributeError:
            self._tempdir = QTemporaryDir()
            if not self._tempdir.isValid():
                raise Exception("Couldn't create temporary directory")
        return self._tempdir.path()

    def remove_tempdir(self):
        try:
            self._tempdir.remove()
            del self._tempdir
        except:
            pass

    def canInsertFromMimeData(self, source):
        if source.hasImage():
           return True
        else:
           return QTextEdit.canInsertFromMimeData(source)

    def closeEvent(self, e):
        if self.maybeSave():
            e.accept()
            self.remove_tempdir()
        else:
            e.ignore()

    def setupFileActions(self):
        tb = QToolBar(self)
        tb.setWindowTitle(self.tr("File Actions"))
        self.addToolBar(tb)

        menu = QMenu(self.tr("&File"), self)
        self.menuBar().addMenu(menu)

        self.actionNew = QAction(
                QIcon.fromTheme('document-new',
                        QIcon(rsrcPath + '/filenew.png')),
                self.tr("&New"), self, priority=QAction.LowPriority,
                shortcut=QKeySequence.New, triggered=self.fileNew)
        tb.addAction(self.actionNew)
        menu.addAction(self.actionNew)

        self.actionOpen = QAction(
                QIcon.fromTheme('document-open',
                        QIcon(rsrcPath + '/fileopen.png')),
                self.tr("&Open..."), self, shortcut=QKeySequence.Open,
                triggered=self.fileOpen)
        tb.addAction(self.actionOpen)
        menu.addAction(self.actionOpen)
        menu.addSeparator()

        self.actionSave = QAction(
                QIcon.fromTheme('document-save',
                        QIcon(rsrcPath + '/filesave.png')),
                self.tr("&Save"), self, shortcut=QKeySequence.Save,
                triggered=self.fileSave, enabled=False)
        tb.addAction(self.actionSave)
        menu.addAction(self.actionSave)

        self.actionSaveAs = QAction(self.tr("Save &As..."), self,
                priority=QAction.LowPriority,
                shortcut=Qt.CTRL + Qt.SHIFT + Qt.Key_S,
                triggered=self.fileSaveAs)
        menu.addAction(self.actionSaveAs)
        menu.addSeparator()

        self.actionProperties = QAction(
                QIcon.fromTheme('preferences-other', QIcon(rsrcPath + '/editcut.png')),
                self.tr("Document properties"), self, priority=QAction.LowPriority,
                shortcut=QKeySequence.Preferences)
        menu.addAction(self.actionProperties)
        menu.addSeparator()

        self.actionPrintPdf = QAction(
                QIcon.fromTheme('exportpdf',
                        QIcon(rsrcPath + '/exportpdf.png')),
                self.tr("Export P&DF..."), self, priority=QAction.LowPriority,
                shortcut=Qt.CTRL + Qt.Key_D,
                triggered=self.filePrintPdf)
        tb.addAction(self.actionPrintPdf)
        menu.addAction(self.actionPrintPdf)
        menu.addSeparator()

        self.actionQuit = QAction(self.tr("&Quit"), self, shortcut=QKeySequence.Quit,
                triggered=self.close)
        menu.addAction(self.actionQuit)

    def setupEditActions(self):
        tb = QToolBar(self)
        tb.setWindowTitle(self.tr("Edit Actions"))
        self.addToolBar(tb)

        menu = QMenu(self.tr("&Edit"), self)
        self.menuBar().addMenu(menu)

        self.actionUndo = QAction(
                QIcon.fromTheme('edit-undo',
                        QIcon(rsrcPath + '/editundo.png')),
                self.tr("&Undo"), self, shortcut=QKeySequence.Undo)
        tb.addAction(self.actionUndo)
        menu.addAction(self.actionUndo)

        self.actionRedo = QAction(
                QIcon.fromTheme('edit-redo',
                        QIcon(rsrcPath + '/editredo.png')),
                self.tr("&Redo"), self, priority=QAction.LowPriority,
                shortcut=QKeySequence.Redo)
        tb.addAction(self.actionRedo)
        menu.addAction(self.actionRedo)
        menu.addSeparator()

        self.actionCut = QAction(
                QIcon.fromTheme('edit-cut', QIcon(rsrcPath + '/editcut.png')),
                self.tr("Cu&t"), self, priority=QAction.LowPriority,
                shortcut=QKeySequence.Cut)
        tb.addAction(self.actionCut)
        menu.addAction(self.actionCut)

        self.actionCopy = QAction(
                QIcon.fromTheme('edit-copy',
                        QIcon(rsrcPath + '/editcopy.png')),
                self.tr("&Copy"), self, priority=QAction.LowPriority,
                shortcut=QKeySequence.Copy)
        tb.addAction(self.actionCopy)
        menu.addAction(self.actionCopy)

        self.actionPaste = QAction(
                QIcon.fromTheme('edit-paste',
                        QIcon(rsrcPath + '/editpaste.png')),
                self.tr("&Paste"), self, priority=QAction.LowPriority,
                shortcut=QKeySequence.Paste,
                enabled=((len(QApplication.clipboard().text()) != 0) or QApplication.clipboard().mimeData().hasImage()))
        tb.addAction(self.actionPaste)
        menu.addAction(self.actionPaste)
        menu.addSeparator()

        self.actionSettings = QAction(
                QIcon.fromTheme('preferences-other', QIcon(rsrcPath + '/editcut.png')),
                self.tr("Settings"), self, priority=QAction.LowPriority,
                shortcut=QKeySequence.Preferences)
        menu.addAction(self.actionSettings)

    def setupTextActions(self):
        tb = QToolBar(self)
        tb.setWindowTitle(self.tr("Format Actions"))
        self.addToolBar(tb)

        menu = QMenu(self.tr("F&ormat"), self)
        self.menuBar().addMenu(menu)

        self.actionTextBold = QAction(
                QIcon.fromTheme('format-text-bold',
                        QIcon(rsrcPath + '/textbold.png')),
                self.tr("&Bold"), self, priority=QAction.LowPriority,
                shortcut=Qt.CTRL + Qt.Key_B, triggered=self.textBold,
                checkable=True)
        bold = QFont()
        bold.setBold(True)
        self.actionTextBold.setFont(bold)
        tb.addAction(self.actionTextBold)
        menu.addAction(self.actionTextBold)
# TODO Fix the image link
        self.actionTextSuperScript = QAction(
                QIcon(':/images/win/textexponent.png'),
                self.tr("&Superscript"), self, priority=QAction.LowPriority,
                triggered=self.textSuperScript,
                checkable=True)
        tb.addAction(self.actionTextSuperScript)
        menu.addAction(self.actionTextSuperScript)

        # TODO Fix the image link
        self.actionTextSubScript = QAction(
                QIcon(':/images/win/textsubscript.png'),
                self.tr("&Subscript"), self, priority=QAction.LowPriority,
                triggered=self.textSubScript,
                checkable=True)
        tb.addAction(self.actionTextSubScript)
        menu.addAction(self.actionTextSubScript)

        self.actionTextItalic = QAction(
                QIcon.fromTheme('format-text-italic',
                        QIcon(rsrcPath + '/textitalic.png')),
                self.tr("&Italic"), self, priority=QAction.LowPriority,
                shortcut=Qt.CTRL + Qt.Key_I, triggered=self.textItalic,
                checkable=True)
        italic = QFont()
        italic.setItalic(True)
        self.actionTextItalic.setFont(italic)
        tb.addAction(self.actionTextItalic)
        menu.addAction(self.actionTextItalic)

        self.actionTextUnderline = QAction(
                QIcon.fromTheme('format-text-underline',
                        QIcon(rsrcPath + '/textunder.png')),
                self.tr("&Underline"), self, priority=QAction.LowPriority,
                shortcut=Qt.CTRL + Qt.Key_U, triggered=self.textUnderline,
                checkable=True)
        underline = QFont()
        underline.setUnderline(True)
        self.actionTextUnderline.setFont(underline)
        tb.addAction(self.actionTextUnderline)
        menu.addAction(self.actionTextUnderline)

        menu.addSeparator()

        tb = QToolBar(self)
        tb.setAllowedAreas(Qt.TopToolBarArea | Qt.BottomToolBarArea)
        tb.setWindowTitle(self.tr("Format Actions"))
        self.addToolBarBreak(Qt.TopToolBarArea)
        self.addToolBar(tb)

        self.comboStyle = QComboBox(tb)
        tb.addWidget(self.comboStyle)
        self.comboStyle.addItem(self.tr("Standard"))
        self.comboStyle.addItem(self.tr("Bullet List (Disc)"))
        self.comboStyle.addItem(self.tr("Bullet List (Dash)"))
        self.comboStyle.addItem(self.tr("Ordered List (Decimal)"))
        self.comboStyle.addItem(self.tr("Ordered List (Alpha lower)"))
        self.comboStyle.addItem(self.tr("Ordered List (Alpha upper)"))
        self.comboStyle.addItem(self.tr("Ordered List (Roman lower)"))
        self.comboStyle.addItem(self.tr("Ordered List (Roman upper)"))
        self.comboStyle.activated.connect(self.textStyle)

    def settings(self):
        # Set up the settings interface from Designer.
        self.settingsDialog = SettingsDialog()
        self.settingsDialog.ui.fontSizeComboBox.activated[str].connect(self.textSize)
        self.settingsDialog.ui.fontName.activated[str].connect(self.textFamily)
        self.settingsDialog.ui.alignmentComboBox.activated[str].connect(self.textAlignment)
        for s in range(self.settingsDialog.ui.fontSizeComboBox.count()):
            if float(self.settingsDialog.ui.fontSizeComboBox.itemText(s)) >= self.fontSize:
                break
        self.settingsDialog.ui.fontSizeComboBox.setCurrentIndex(s)
        self.settingsDialog.ui.fontName.setCurrentText(self.fontName)
        self.settingsDialog.ui.alignmentComboBox.setCurrentText(self.alignment)
        self.settingsDialog.ui.indentationLineEdit.setText(str(self.indentation))
        self.settingsDialog.ui.interlineLineEdit.setText(str(self.interline))
        self.settingsDialog.ui.buttonBox.button(QDialogButtonBox.Apply).clicked.connect(self.applySettings)
        self.settingsDialog.ui.buttonBox.accepted.connect(self.acceptSettings)
        self.settingsDialog.ui.fopBasePathLineEdit.setText(self.fop_path)
        self.settingsDialog.ui.fopCommandLineEdit.setText(self.fop_cmd)
        self.settingsDialog.ui.imageResolutionLineEdit.setText(str(self.resolution))
        self.settingsDialog.ui.docModelLineEdit.setText(self.newDocument)
        self.settingsDialog.exec_()

    def acceptSettings(self):
        self.generalSettings()
        self.editorSettings()

    def applySettings(self):
        if self.settingsDialog.ui.tabWidget.currentIndex() == 0:    # General tab
            self.generalSettings()
        elif self.settingsDialog.ui.tabWidget.currentIndex() == 1:    # Editor tab
            self.editorSettings()

    def generalSettings(self):
        self.mysettings.setValue("General/foppath", self.settingsDialog.ui.fopBasePathLineEdit.text())
        self.fop_path = self.settingsDialog.ui.fopBasePathLineEdit.text()
        self.mysettings.setValue("General/fopcommand", self.settingsDialog.ui.fopCommandLineEdit.text())
        self.fop_cmd = self.settingsDialog.ui.fopCommandLineEdit.text()
        self.mysettings.setValue("Display/resolution", self.settingsDialog.ui.imageResolutionLineEdit.text())
        self.resolution = float(self.settingsDialog.ui.imageResolutionLineEdit.text())
        #TODO redraw images with new resolution
        self.newDocument = self.settingsDialog.ui.docModelLineEdit.text()
        self.mysettings.setValue("General/docmodel", self.settingsDialog.ui.docModelLineEdit.text())

    def editorSettings(self):
        self.indentation = float(self.settingsDialog.ui.indentationLineEdit.text())
        self.interline = float(self.settingsDialog.ui.interlineLineEdit.text())
        self.fontName = self.settingsDialog.ui.fontName.currentText()
        self.fontSize = int(self.settingsDialog.ui.fontSizeComboBox.currentText())
        self.alignment = self.settingsDialog.ui.alignmentComboBox.currentText()
        self.mysettings.setValue("Display/family", self.settingsDialog.ui.fontName.currentText())
        self.mysettings.setValue("Display/size", self.settingsDialog.ui.fontSizeComboBox.currentText())
        self.mysettings.setValue("Display/alignment", self.settingsDialog.ui.alignmentComboBox.currentText())
        self.mysettings.setValue("Display/indentation", self.settingsDialog.ui.indentationLineEdit.text())
        self.mysettings.setValue("Display/interline", self.settingsDialog.ui.interlineLineEdit.text())
        # Apply the new settings to current document
        for chapter in (self.desc_edit, self.claims_edit, self.abstract_edit):
            for i in range(0, chapter.vb.count()):
                chapter.vb.itemAt(i).widget().layout().itemAt(0).widget().setFormat()

    def properties(self):
        # Set up the user interface from Designer.
        self.propertiesDialog = PropertiesDialog()
        self.propertiesDialog.ui.buttonBox.accepted.connect(self.acceptProperties)
        self.propertiesDialog.ui.buttonBox.rejected.connect(self.rejectProperties)
        #Populate fields
        self.propertiesDialog.ui.languageLineEdit.setText(self.lang)
        self.propertiesDialog.ui.countryLineEdit.setText(self.country)
        self.propertiesDialog.ui.dtdNameLineEdit.setText(self.dtd)
        self.propertiesDialog.ui.dtdVersionLineEdit.setText(self.dtd_version)
        self.propertiesDialog.exec_()

    def acceptProperties(self):
        self.lang = self.propertiesDialog.ui.languageLineEdit.text()
        self.country = self.propertiesDialog.ui.countryLineEdit.text()
        self.dtd = self.propertiesDialog.ui.dtdNameLineEdit.text()
        self.dtd_version = self.propertiesDialog.ui.dtdVersionLineEdit.text()

    def rejectProperties(self):
        pass

    def textSize(self, pointSize):
        pointSize = float(pointSize)
        if pointSize > 0:
            fmt = QTextCharFormat()
            fmt.setFontPointSize(pointSize)
            self.mergeFormatDocument(fmt)

    def insertAsHtml(self, cursor, node):
        '''
        This function is used when reading a file. Convert a section or a para as html and insert it in the text
        :param cursor: position where to insert the text
        :param node: root node of the fragment from xml file to insert
        :return: void
        '''
        # look for images. Replace the src link.
        figlist = node.elementsByTagName("img")
        for i in range(figlist.count()):
            figitem = figlist.at(i).toElement()
            path = figitem.attribute("file")
            path = os.path.join(self.tempdir(),path)
            image = QImage(path)
            if not image.isNull():
                he = figitem.attribute("he")
                wi = figitem.attribute("wi")
                img_format = figitem.attribute("img-format")
                orientation = figitem.attribute("orientation")
#                id = figitem.attribute("file")
                parent = figitem.parentNode().toElement()
                category = ""
                num = 0
                if parent.tagName() in ("tables","chemistry","maths"):
                    category = parent.tagName()
                    num = int(parent.attribute("num"))
                #cursor.block().setUserData(MetaData(wi=wi, he=he, img_format=img_format, orientation=orientation))
                self.image_data.addImageData(path, num, he,wi,orientation,img_format, category)
                figitem.setAttribute("src",path)
                figitem.setAttribute("height", int(float (he) * self.resolution ))
                figitem.setAttribute("width", int( float (wi) * self.resolution ))
            else:
                QMessageBox.warning(self, self.tr("Loading"),
                                    self.tr("The image %s can't be found.\n") % figitem.attribute("file"),
                                    QMessageBox.Ok)
        startpos = cursor.position()
        b = QByteArray()
        ts = QTextStream(b)
        node.save(ts, 0)
        ts.readAll()
        cursor.insertFragment(QTextDocumentFragment.fromHtml(str(b.data(),
                                                    encoding='utf-8')))
        endpos = cursor.position()
        cursor.setPosition(startpos,QTextCursor.KeepAnchor)
        cursor.mergeCharFormat(self.charfmt)
        cursor.mergeBlockFormat(self.blockfmt)

        cursor.setPosition(endpos,QTextCursor.MoveAnchor)


    def extract(self, f):
        '''
        Extract the zip archive in temp directory.
        :param f string: the archive name
        :return the name of application-body.xml in temp dir.
        '''
        with zipfile.ZipFile(f) as zipf:
            zipf.extractall(self.tempdir())
            #        self.textEdit.document().setModified(False)   TODO
        return QFile(os.path.join(self.tempdir(),"application-body.xml"))

    def clear(self):
        '''
        Reset all tabs
        '''
        for tab in (self.desc_edit, self.claims_edit, self.abstract_edit, self.drawings_edit):
            tab.clear()

    def load(self, f):
        '''
        Load the document. If extension is zip, the archive is extracted in a temp file.
        :param f: string: the document name, including path
        :return: bool: True if the document has been loaded
        '''

        if not QFile.exists(f):
            print(self.tr("File not found"))
            return False

        fh = QFile(f)
        ff = f
        self.remove_tempdir()
        if QFileInfo(fh).suffix()=="zip":
            fh = self.extract(QFileInfo( fh).absoluteFilePath())
            ff = fh.fileName()
        if not fh.open(QFile.ReadOnly):
            print(self.tr("File not found"))
            return False
        if self.fop_path != "" :
            # we don't check the model, fop_path may not be defined, we should be able to start
            from lxml import etree
            with open(ff,'r') as ffo:
                fc = etree.parse(ffo)
                info = fc.docinfo
                self.dtd = info.system_url
                if os.path.isfile(os.path.join(self.fop_path,self.dtd)):
                    dtdvalidator = etree.DTD(os.path.join(self.fop_path,self.dtd))
                    if dtdvalidator.validate(fc):
                        print("DTD passed")
                    else:
                        print(dtdvalidator.error_log.filter_from_errors()[0])
                        ret = QMessageBox.warning(self, self.tr("Document loading"), \
                                            self.tr("The document doesn't pass DTD validation.\n \
                                    Do you want to continue?"),
                                                  QMessageBox.Ok | QMessageBox.Cancel)
                        if not ret:
                            return False
                else:
                        ret = QMessageBox.warning(self, self.tr("Document loading"), \
                                            self.tr("The DTD {dtd} is not installed in {path}.\n \
                                    Do you want to continue?".format(dtd=self.dtd, path=self.fop_path)),
                                                  QMessageBox.Ok | QMessageBox.Cancel)
                        if not ret:
                            return False

        # Apply editor settings
        self.charfmt = QTextCharFormat()
        self.charfmt.setFontFamily(self.fontName)
        self.charfmt.setFontPointSize(self.fontSize)

        self.blockfmt = QTextBlockFormat()
        self.blockfmt.setAlignment(self.qtalign[self.alignment])
        self.blockfmt.setTextIndent(self.indentation)
        self.blockfmt.setLineHeight(self.interline, 4)  # 4: LineDistanceHeight

        doc = QDomDocument()
        doc.setContent(fh)
        app_body = doc.documentElement()

        e = app_body.toElement()
        if (e) :
            if (e.tagName() !=  "application-body") :
                raise Exception("This file is not a conform patent application")
            else:
                try:
                    self.lang = e.attribute("lang")
                except:
                    self.lang = ""
                try:
                    self.country = e.attribute("country")
                except:
                    self.country = "IB"
                try:
                    self.dtd_version = e.attribute("dtd-version")
                except:
                    self.dtd_version = "1.7"
        # Replace figref occurences
        figlist = e.elementsByTagName("figref")
        while (figlist.count() != 0):
            figitem = figlist.at(0).toElement()
            newspan = doc.createElement("span")
            newtext = doc.createTextNode(self.tr("Fig. %s%s")%
                        (figitem.attribute("num"),figitem.text()))
            newspan.appendChild(newtext)
            figlist.at(0).parentNode().replaceChild(newspan, figlist.at(0))

        cns = e.childNodes()
        for icn in range(0, cns.count()):
            cn = cns.at(icn).toElement()
            aname = cn.tagName()
            if (aname == "description" ):
                try:
                    self.desc_edit.lang = cn.attribute("lang")
                except:
                    pass
                a = cn.childNodes()
                for ia in range(0, a.count()):
                    an = a.at(ia).toElement()
                    aname = an.tagName()
                    if aname in ("invention-title","technical-field","background-art",
                                 "disclosure","description-of-drawings","mode-for-invention"):
                        desc_cursor, _ = self.desc_edit.addSection(aname)
                        self.insertAsHtml(desc_cursor,an)
                    elif(aname == "p"):
                        if an.firstChild().toElement().tagName() == "ul":
                            self.insertAsHtml(desc_cursor, an.firstChild())
                        else:
                            self.insertAsHtml(desc_cursor, an)
                    elif (aname == "figref"):
                        desc_cursor.insertText("Fig. %d "%(an.attribute("num")), self.charfmt)
                        self.insertAsHtml(desc_cursor, an)
            elif (aname == "claims"):
                a = cn.childNodes()
                for ia in range(0, a.count()):
                    an = a.at(ia).toElement()
                    aname = an.tagName()
                    if (aname == "claim"):
                        claims_cursor, _ = self.claims_edit.addClaim(an.attribute("num"))
                        d = an.childNodes()
                        for id in range(0, d.count()):
                            dn = d.at(id).toElement()
                            dname = dn.tagName()
                            if (dname == "claim-text"):
                                self.insertAsHtml(claims_cursor, dn)
            elif (aname == "abstract"):
                abstract_cursor, _ = self.abstract_edit.addSection("abstract")
                blockfmt = abstract_cursor.blockFormat()
#                abstract_cursor = self.abstract_edit.document().rootFrame().lastCursorPosition()
#                self.abstract_edit.sections[aname] = abstract_cursor.insertFrame(self.desc_edit.defaultFrameFormat)
                a = cn.childNodes()
                for ia in range(0, a.count()):
                    startpos = abstract_cursor.position()
                    an = a.at(ia).toElement()
                    b = QByteArray()
                    ts = QTextStream(b)
                    an.save(ts, 2)
                    ts.readAll()
                    abstract_cursor.insertFragment(QTextDocumentFragment.fromHtml(str(b.data(),
                                            encoding='utf-8')))
                    endpos = abstract_cursor.position()
                    abstract_cursor.setPosition(startpos, QTextCursor.KeepAnchor)
                    abstract_cursor.mergeCharFormat(self.charfmt)
                    abstract_cursor.mergeBlockFormat(blockfmt)
                    abstract_cursor.setPosition(endpos,QTextCursor.MoveAnchor)
                    abstract_cursor.insertBlock()

            elif (aname == "drawings"):
                #self.drawings_edit.sections[aname] = drawings_cursor.insertFrame(self.desc_edit.defaultFrameFormat)
                #drawingsFrame = drawings_cursor.currentFrame()
                a = cn.childNodes()
                for ia in range(0, a.count()):
                    an = a.at(ia).toElement()
                    if(an.tagName() == "figure"):
                        ac = an.firstChild().toElement()
                        if ac.tagName() == "img":
                            path = ac.attribute("file")
                            path = os.path.join(self.tempdir(),path)
                            image = QImage(path)
                            if not image.isNull() :
                                he = ac.attribute("he")
                                wi = ac.attribute("wi")
                                img_format = ac.attribute("img-format")
                                orientation = ac.attribute("orientation")
                                img_id = ac.attribute("file")
                                #self.drawings_edit.doc(0).addResource(QTextDocument.ImageResource, QUrl(path), image)
#                                qtif = QTextImageFormat()
#                                qtif.setName(path)
#                                qtif.setHeight(float(he) * self.drawings_edit.resolution)
                                pixmap = QPixmap()
                                pixmap = pixmap.fromImage(image.scaledToWidth(float(wi) * self.resolution))
                                self.drawings_edit.addDrawing(self.tr("Figure %s")%an.attribute("num"), pixmap, img_id)
                                self.image_data.addImageData(ac.attribute("file"), an.attribute("num"), he, wi, orientation, img_format, "figure")
                            else:
                                QMessageBox.warning(self, self.tr("Loading"),
                                                    self.tr("The image %s can't be found.\n")%ac.attribute("file"),
                                                     QMessageBox.Ok)
        #  Check if there is images in drawings chapter. If none, add widget for insertion
        if self.drawings_edit.vb.count() == 0:
            image = QPixmap()
            self.drawings_edit.addDrawing(self.tr("The application has no figure yet."), image, "dummy_id")
        self.setModified(False)
        return True

    def maybeSave(self):
        if not self.isWindowModified():
            return True

        if self.fileName.startswith(':/'):
            return True

        ret = QMessageBox.warning(self, APP,
                self.tr("The document has been modified.\nDo you want to save your changes?"),
                QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)

        if ret == QMessageBox.Save:
            return self.fileSave()

        if ret == QMessageBox.Cancel:
            return False

        return True

    def setCurrentFileName(self, fileName=''):
        self.fileName = fileName

        if not fileName:
            shownName = 'untitled'
        else:
            shownName = QFileInfo(fileName).fileName()

        self.setWindowTitle(self.tr("%s[*] - %s") % (shownName, APP))
        self.setModified(False)

    def fileNew(self):
        if self.maybeSave():
            self.clear()
            self.setCurrentFileName()
            self.load(self.newDocument)
            self.setWindowModified(False)

    def fileOpen(self):
        if self.maybeSave():
            fn, _ = QFileDialog.getOpenFileName(self, self.tr("Open File..."), None,
                    self.tr("Application (*.xml *.zip);;All Files (*)"))

            if fn:
                self.image_data.reset()
                self.clear()
                self.load(fn)
                self.setCurrentFileName(fn)
                for i in range(0, len(self.desc_edit.vb)):
                    self.desc_edit.vb.itemAt(i).widget().layout().itemAt(0).widget().document().adjustSize()
                self.tabs.setCurrentIndex(1)
                self.tabs.setCurrentIndex(0)

    def fileSave(self):
        if not self.fileName:
            return self.fileSaveAs()
        QApplication.setOverrideCursor(Qt.WaitCursor)

        dom = self.builddom()
        file = QFile(os.path.join(self.tempdir(),\
                                  "application-body.xml"))
        try:
            file.open(QIODevice.WriteOnly | QIODevice.Text)
            file_stream = QTextStream(file)
            dom.save(file_stream, 4)
            if QFileInfo(self.fileName).suffix() != "zip":
                name = self.fileName+ ".zip"
            else:
                name = self.fileName
            file.close()
            zipf = zipfile.ZipFile(name , 'w', zipfile.ZIP_DEFLATED)
            zipf.write(os.path.join(self.tempdir(), \
                         "application-body.xml"),"application-body.xml")
            figlist = dom.elementsByTagName("img")
            for i in range(figlist.count()):
                figitem = figlist.at(i).toElement()
                path = figitem.attribute("file")
                zipf.write(os.path.join(self.tempdir(),path), path)
            # for root, dirs, files in os.walk(self.textEdit.tempdir()):
            #     for file in files:
            #         zipf.write(os.path.join(root, file), file)
            zipf.close()
            self.setModified(False)
#            self.textEdit.document().setModified(False)
        except:
            QMessageBox.critical(self, self.tr("Save"), self.tr("Unable to save the document"))
            QApplication.restoreOverrideCursor()
            return False
        QApplication.restoreOverrideCursor()
        self.actionSave.setEnabled(False)
        self.setModified(False)
        return True

    def fileSaveAs(self):
        fn, _ = QFileDialog.getSaveFileName(self, self.tr("Save as..."), None,
                self.tr("Zip-Files (*.zip);;All Files (*)"))

        if not fn:
            return False

        lfn = fn.lower()
        if not lfn.endswith(('.zip', '.xml')):
            # The default.
            fn += '.zip'

        self.setCurrentFileName(fn)
        return self.fileSave()

    def builddom(self):
        '''
        Convert the document in a DOM tree to save it as XML
        :return:
        the DOM object
        '''
        impl = QDomImplementation()
        typ = impl.createDocumentType("application-body", None,
                                self.dtd)
        domwriter = QDomDocument(typ)
        # Insert processing instructions : are they usefull?
        xml_node = domwriter.createProcessingInstruction("xml",
                                    "version=\"1.0\" encoding=\"UTF-8\"")
        previous = domwriter.insertBefore(xml_node, domwriter.firstChild())
        xml_node = domwriter.createProcessingInstruction("software_name", "patibody")
        domwriter.insertAfter(xml_node, previous)
        xml_node = domwriter.createProcessingInstruction("software_version", "0.1")
        domwriter.insertAfter(xml_node, previous)
        app_body = domwriter.createElement("application-body")
        app_body.setAttribute("country", self.country)
        app_body.setAttribute("lang", self.lang)
        app_body.setAttribute("dtd-version", self.dtd_version)
        domwriter.appendChild(app_body)
        previous_tag = ""
        for chapter in (self.desc_edit, self.claims_edit, self.abstract_edit, self.drawings_edit):
            tag = chapter.tag
            if chapter in (self.desc_edit, self.abstract_edit):
                num = 1
                descNode = domwriter.createElement(tag)
                for i in range(0, chapter.vb.count()):
                    te = chapter.vb.itemAt(i).widget().layout().itemAt(0).widget()
                    if chapter == self.abstract_edit:
                        newNode = descNode
                    else:
                        newNode = domwriter.createElement(te.tag)
                        descNode.appendChild(newNode)
                    cursor = te.textCursor()
                    cursor.movePosition(QTextCursor.Start)
                    while 1:
                        if te.tag == 'invention-title':
                            inpara = False
                        else:
                            inpara = True
                        num, previous_tag = self.insertDomPara(cursor,num,newNode, previous_tag, inpara)
                        if not cursor.movePosition(QTextCursor.NextBlock):
                            # We leave description/abstract chapter
                            break
                app_body.appendChild(descNode)
            if tag == 'claims':
                claimNode = domwriter.createElement(tag)
                app_body.appendChild(claimNode)
                for i in range(0, chapter.vb.count()):
                    te = chapter.vb.itemAt(i).widget().layout().itemAt(0).widget()
                    newNode = domwriter.createElement('claim')
                    newNode.setAttribute("num", i + 1)
                    claimNode.appendChild(newNode)
                    cursor = te.textCursor()
                    cursor.movePosition(QTextCursor.Start)
                    while 1:
                        num, previous_tag = self.insertDomPara(cursor, 0, newNode, previous_tag, inpara=True, claim_text=True)
                        if not cursor.movePosition(QTextCursor.NextBlock):
                            # We leave claim
                            break
            if chapter == self.drawings_edit:
                drawingsNode = domwriter.createElement('drawings')
                for i in range(0, chapter.vb.count()):
                    image_name = chapter.vb.itemAt(i).widget().img_id
                    if image_name == "dummy_id":
                        break
                    newnode = domwriter.createElement("figure")
                    newnode.setAttribute("num", i + 1)
                    imgNode = domwriter.createElement("img")
                    imgNode.setAttribute("file",os.path.basename(image_name))
                    imgNode.setAttribute("he",self.image_data.height(image_name))
                    imgNode.setAttribute("wi",self.image_data.width(image_name))
                    imgNode.setAttribute("img-format",self.image_data.img_format(image_name))
                    imgNode.setAttribute("img-content",'drawing')
                    imgNode.setAttribute("orientation",self.image_data.orientation(image_name))
                    newnode.appendChild(imgNode)
                    drawingsNode.appendChild(newnode)
                if image_name != "dummy_id":
                    app_body.appendChild(drawingsNode)

        return domwriter

    def createImgNode(self, image_name):
        imgNode = QDomDocument().createElement("img")
        imgNode.setAttribute("file", os.path.basename(image_name))
        imgNode.setAttribute("img-format", self.image_data.img_format(image_name))
        imgNode.setAttribute("wi", self.image_data.width(image_name))
        imgNode.setAttribute("he", self.image_data.height(image_name))
        imgNode.setAttribute("orientation", self.image_data.orientation(image_name))
        return imgNode

    def insertDomPara(self,cursor, num, node, previous_tag, inpara = True, claim_text=False):
        ''' Get the content in HTML format from QTextDocument class to insert in Dom structure.
        We suppress style attribute in p elements.
        We replace style formatted span elements with proper tag:
        <span style=" font-weight:600;"> for bold : <b>
        <span style=" font-style:italic;"> for italic : <i>
        <span style=" text-decoration: underline;"> for underline : <u>
        <span style="vertical-align:super"> for superscript : <sup>
        <span style="vertical-align:sub"> for superscript : <sub>
        sub2 sup2 smallcaps pre : not used

        Variables used:
        previous_tag: used for ul and ol to add newline in the same list
        inpara: says that text has not to be in a "p" tag
        extract: a portion of HTML code between body tags from the block under the cursor
        extract_doc_dom = QDomDocument instance containing extract as DOM hierarchy
        extract_dom = first node of extract_doc_dom, containing p element
        cn = list of child nodes with span tag
        '''
        if len(cursor.block().text()) == 0:
            return num, previous_tag
        extract_doc_dom = QDomDocument()

        if cursor.charFormat().isImageFormat():
            imgFmt = cursor.charFormat().toImageFormat()
            image_name = imgFmt.name()
            if claim_text:
                newNode = extract_doc_dom.createElement("claim-text")
            else:
                newNode = extract_doc_dom.createElement("p")
                newNode.setAttribute("num", num)
                num +=1
            cat = self.image_data.category(image_name)
            imgNode = self.createImgNode(image_name)
            if cat != "":
                catNode = extract_doc_dom.createElement(cat)
                catNode.setAttribute("num",  self.image_data.num(image_name))
                catNode.appendChild(imgNode)
                newNode.appendChild(catNode)
            else:
                newNode.appendChild(imgNode)
            node.appendChild(newNode)
            current_tag = newNode
        else:
            cursor.movePosition(QTextCursor.EndOfBlock, QTextCursor.KeepAnchor)
            html = cursor.selection().toHtml()
            extract = (html.split("<body>")[1]).split("</body>")[0]
            extract = extract.replace("<!--StartFragment-->","")
            extract = extract.replace("<!--EndFragment-->","")
            extract = extract.replace("style=\"\"","")
            # Delete paragraph which contains only a br
            regex = r"<p [a-zA-Z0-9=:;\s\-\"]*><br \/><\/p>"
            extract = re.sub(regex, "",extract)
            # Delete br
            extract = extract.replace("<br />"," ")
            extract_doc_dom.setContent("<div>" + extract + "</div>")
            cn = extract_doc_dom.elementsByTagName("img")
            for i in range(cn.count(), 0, -1):
                img = cn.at(i-1).toElement()
                image_name = img.attribute('src')
                imgNode = self.createImgNode(image_name)
                cn.at(i-1).parentNode().replaceChild(imgNode, img)
            cn = extract_doc_dom.elementsByTagName("span")
            for i in range(cn.count(), 0, -1):
                bold = False
                italic = False
                underline = False
                superscript = False
                subscript = False
                span = cn.at(i-1).toElement()
                style = span.attribute("style")
                if style.find("font-weight:")>=0:
                    bold = True
                if style.find("font-style:italic") >=0:
                    italic = True
                if style.find("text-decoration: underline")>=0:
                    underline = True
                if style.find("vertical-align:super")>=0:
                    superscript = True
                if style.find("vertical-align:sub")>=0:
                    subscript = True
                replaced = False
                if bold or italic or underline or superscript or subscript :
                    if bold:
                        newnode = extract_doc_dom.createElement("b")
                        startnode = newnode
                        currentnode = newnode
                        replaced = True
                    if italic:
                        newnode = extract_doc_dom.createElement("i")
                        if replaced:
                            currentnode.appendChild(newnode)
                        else:
                            cn.at(i).parentNode().appendChild(newnode)
                            replaced = True
                            startnode = newnode
                        currentnode = newnode
                    if underline:
                        newnode = extract_doc_dom.createElement("u")
                        if replaced:
                            currentnode.appendChild(newnode)
                        else:
                            cn.at(i).parentNode().appendChild(newnode)
                            replaced = True
                            startnode = newnode
                        currentnode = newnode
                    if superscript:
                        newnode = extract_doc_dom.createElement("sup")
                        if replaced:
                            currentnode.appendChild(newnode)
                        else:
                            cn.at(i).parentNode().appendChild(newnode)
                            replaced = True
                            startnode = newnode
                        currentnode = newnode
                    if subscript:
                        newnode = extract_doc_dom.createElement("sub")
                        if replaced:
                            currentnode.appendChild(newnode)
                        else:
                            cn.at(i).parentNode().appendChild(newnode)
                            replaced = True
                            startnode = newnode
                        currentnode = newnode
                    newtext = extract_doc_dom.createTextNode(span.text())
                    currentnode.appendChild(newtext)
                    cn.at(i-1).parentNode().replaceChild(startnode, span)
                else:
                    # replace span with his text context, span is not dealt
                    newtext = extract_doc_dom.createTextNode(span.text())
                    cn.at(i - 1).parentNode().replaceChild(newtext, span)
            # Suppress style attributes from li tags
            cn = extract_doc_dom.elementsByTagName("li")
            for i in range( 0,cn.count()):
                cn.at(i).toElement().removeAttribute("style")
            # Suppress align attributes from p tags
            cn = extract_doc_dom.elementsByTagName("p")
            for i in range( 0,cn.count()):
                cn.at(i).toElement().removeAttribute("align")
            # Add the content of block to writer
            fc = extract_doc_dom.firstChild()
            ecn = fc.childNodes()
            c = ecn.count()
            for n in range(0, c):
                extract_dom = ecn.at(0)
                # Always at(0) because previous child is removed
                extract_dom.toElement().removeAttribute("style")
                current_tag = extract_dom.toElement().tagName()
                if current_tag != "p":
                    # get list format
                    cur_list = cursor.currentList()
                    if cur_list:
                        style = cur_list.format().style()
                        try:
                            list_type = self.listTypeDict[style]
                        except:
                            list_type = ""
                    if current_tag == "ul":
                        attribute_name ="list-style"
                    if current_tag == "ol":
                        attribute_name ="ol-style"
                    if current_tag == previous_tag and (current_tag=="ul" or current_tag=="ol" ):
                        # We add a new li to the list
                        newnode = extract_dom.firstChild()
                        # node is the ul/ol parent
                        node.lastChild().lastChild().appendChild(newnode)
                    elif claim_text:
                        newnode = extract_doc_dom.createElement("claim-text")
                        if list_type != "" :
                            extract_dom.toElement().setAttribute(attribute_name, list_type)
                        newnode.appendChild(extract_dom)
                        node.appendChild(newnode)
                    else:
                        newnode = extract_doc_dom.createElement("p")
                        newnode.setAttribute('num',num)
                        num +=1
                        if list_type != "" :
                            extract_dom.toElement().setAttribute(attribute_name, list_type)
                        newnode.appendChild(extract_dom)
                        node.appendChild(newnode)
                else:
                    if inpara:
                        if claim_text:
                            extract_dom.toElement().setTagName("claim-text")
                        else:
                            extract_dom.toElement().setAttribute('num',num)
                            num += 1
                        if node.appendChild(extract_dom).isNull():
                            #self.displayTree(extract_dom)
                            raise Exception(self.tr("Failed to convert %s to XML")% extract)
                    else:
                        if node.appendChild(extract_dom.firstChild()).isNull():
                            raise Exception(self.tr("Failed to convert %s to XML")% extract)

        return num, current_tag

    def filePrintPdf(self):
        if self.isModified():
            ret = QMessageBox.warning(self, self.tr("Document PDF generation"), \
                                  self.tr("The document need to be saved.\n \
                                        Do you want to save it?"),
                                  QMessageBox.Ok | QMessageBox.Cancel)
            if not ret:
                return False
        fn, _ = QFileDialog.getSaveFileName(self, self.tr("Export PDF"), None,
                self.tr("PDF files (*.pdf);;All Files (*)"))
        self.fileSave()
        if fn:
            QApplication.setOverrideCursor(Qt.WaitCursor)
            from subprocess import Popen, PIPE
            if QFileInfo(fn).suffix() == "":
                fn += '.pdf'

            cmd = '{cmd} -xml "{source}" -xsl "{xsl}" "{output}"'\
                .format(cmd = self.fop_cmd,
                        path = self.fop_path,
                        xsl = os.path.join(self.fop_path, "application-body.xfo"),
                        source=os.path.join(self.tempdir(),"application-body.xml"),
                        output=fn)
            pipe = Popen('{ ' + cmd + '; } 2>&1', cwd=self.fop_path, stdin=PIPE, stdout=PIPE,
                                    universal_newlines=True, shell=True)
            of = pipe.stdout.fileno()
            text = ''
            pipe.stdin.close()
            while True:
                text += os.read(of, 8192).decode('utf8')
                status = pipe.poll()
                if status is not None or text == '':
                    break
            if status != 0:
                raise Exception("Error %d exporting to PDF: %s\nCommand was: %s\n"%(status,text,cmd))
            QApplication.restoreOverrideCursor()

    def textBold(self):
        fmt = QTextCharFormat()
        fmt.setFontWeight(self.actionTextBold.isChecked() and QFont.Bold or QFont.Normal)
        self.mergeFormatOnWordOrSelection(fmt)

    def textUnderline(self):
        fmt = QTextCharFormat()
        fmt.setFontUnderline(self.actionTextUnderline.isChecked())
        self.mergeFormatOnWordOrSelection(fmt)

    def textSuperScript(self):
        fmt = QTextCharFormat()
        fmt.setVerticalAlignment(QTextCharFormat.AlignSuperScript if self.actionTextSuperScript.isChecked() else QTextCharFormat.AlignNormal)
        self.mergeFormatOnWordOrSelection(fmt)

    def textSubScript(self):
        fmt = QTextCharFormat()
        fmt.setVerticalAlignment(QTextCharFormat.AlignSubScript if self.actionTextSubScript.isChecked() else QTextCharFormat.AlignNormal)
        self.mergeFormatOnWordOrSelection(fmt)

    def textItalic(self):
        fmt = QTextCharFormat()
        fmt.setFontItalic(self.actionTextItalic.isChecked())
        self.mergeFormatOnWordOrSelection(fmt)

    def textFamily(self, family):
        fmt = QTextCharFormat()
        fmt.setFontFamily(family)
        self.mergeFormatDocument(fmt)

    def textAlignment(self, alignment):
        widget =  self.focusWidget()
        if widget.metaObject().className() != "CTextEdit":
            return
        fmt = QTextBlockFormat()
        fmt.setAlignment(self.qtalign[alignment])
        cursor = widget.textCursor()
        cursor.select(QTextCursor.Document)
        cursor.mergeBlockFormat(fmt)

    def textStyle(self, styleIndex):
        widget =  self.focusWidget()
        if widget.metaObject().className() != "CTextEdit":
            return
        cursor = widget.textCursor()
        self.currentListFormat = styleIndex
        if styleIndex:
            style = self.styleDict.get(styleIndex, QTextListFormat.ListDisc)
            cursor.beginEditBlock()
            blockFmt = cursor.blockFormat()
            listFmt = QTextListFormat()

            if cursor.currentList():
                listFmt = cursor.currentList().format()
            else:
                listFmt.setIndent(blockFmt.indent() + 1)
                blockFmt.setIndent(0)
                cursor.setBlockFormat(blockFmt)

            listFmt.setStyle(style)
            cursor.createList(listFmt)
            cursor.endEditBlock()
        else:
            bfmt = QTextBlockFormat()
            bfmt.setObjectIndex(-1)
            cursor.mergeBlockFormat(bfmt)

    def cursorPositionChanged(self, section):
        widget =  self.focusWidget()
        if widget.metaObject().className() == "CTextEdit":
            #   Check if style is changed
            cur_list = widget.textCursor().currentList()
            if cur_list:
                style = cur_list.format().style()
                if style != self.currentListFormat:
                    # Update style combobox
                    self.currentListFormat = style
                    for index, st in self.styleDict.items():
                        if st == style :
                            break
                    self.comboStyle.setCurrentIndex(index)
            else:
                if self.currentListFormat != None:
                    self.currentListFormat = None
                    # update style combobox to Standard
                    self.comboStyle.setCurrentIndex(0)

    def clipboardDataChanged(self):
        self.actionPaste.setEnabled((len(QApplication.clipboard().text()) != 0) or QApplication.clipboard().mimeData().hasImage())

    def about(self):
        QMessageBox.about(self, self.tr("About"),
                self.tr("PatiBody allows to edit text for patent application \n \
and save it in XML format."))

    def mergeFormatOnWordOrSelection(self, format):
        cursor = self.focusWidget().textCursor()
        if not cursor.hasSelection():
            cursor.select(QTextCursor.WordUnderCursor)

        cursor.mergeCharFormat(format)
        self.focusWidget().mergeCurrentCharFormat(format)

    def mergeFormatDocument(self, format):
        widget =  self.focusWidget()
        if widget.metaObject().className() != "CTextEdit":
            return
        cursor = widget.textCursor()
        cursor.select(QTextCursor.Document)
        cursor.mergeCharFormat(format)
        widget.mergeCurrentCharFormat(format)

    def fontChanged(self, font):
        self.actionTextBold.setChecked(font.bold())
        self.actionTextItalic.setChecked(font.italic())
        self.actionTextUnderline.setChecked(font.underline())

if __name__ == '__main__':
    app = QApplication(sys.argv)

    locale = QLocale.system().name()
    qtTranslator = QTranslator()
    if qtTranslator.load("qt_" + locale,QLibraryInfo.location(QLibraryInfo.TranslationsPath)):
        app.installTranslator(qtTranslator)
    appTranslator = QTranslator()
    if appTranslator.load("patibody_" + locale,':/languages'):
        app.installTranslator(appTranslator)
    mainWindows = []
    for fn in sys.argv[1:] or [None]:
        patEdit = PatEdit(fn)
        patEdit.resize(800, 900)
        patEdit.show()
        patEdit.docResize()
        app.focusChanged.connect(patEdit.editFocusChanged)
        mainWindows.append(patEdit)

    app.exec_()
