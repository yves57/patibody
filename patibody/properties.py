# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'properties.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Properties(object):
    def setupUi(self, Properties):
        Properties.setObjectName("Properties")
        Properties.resize(676, 306)
        self.buttonBox = QtWidgets.QDialogButtonBox(Properties)
        self.buttonBox.setGeometry(QtCore.QRect(10, 260, 621, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Apply|QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayoutWidget = QtWidgets.QWidget(Properties)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 671, 251))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout_3 = QtWidgets.QFormLayout()
        self.formLayout_3.setObjectName("formLayout_3")
        self.countryLabel = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.countryLabel.setObjectName("countryLabel")
        self.formLayout_3.setWidget(6, QtWidgets.QFormLayout.LabelRole, self.countryLabel)
        self.dtdNameLabel = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.dtdNameLabel.setObjectName("dtdNameLabel")
        self.formLayout_3.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.dtdNameLabel)
        self.dtdVersionLabel = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.dtdVersionLabel.setObjectName("dtdVersionLabel")
        self.formLayout_3.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.dtdVersionLabel)
        self.languageLabel = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.languageLabel.setObjectName("languageLabel")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.languageLabel)
        self.countryLineEdit = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.countryLineEdit.setObjectName("countryLineEdit")
        self.formLayout_3.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.countryLineEdit)
        self.dtdVersionLineEdit = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.dtdVersionLineEdit.setObjectName("dtdVersionLineEdit")
        self.formLayout_3.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.dtdVersionLineEdit)
        self.dtdNameLineEdit = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.dtdNameLineEdit.setObjectName("dtdNameLineEdit")
        self.formLayout_3.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.dtdNameLineEdit)
        self.languageLineEdit = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.languageLineEdit.setObjectName("languageLineEdit")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.languageLineEdit)
        self.verticalLayout.addLayout(self.formLayout_3)

        self.retranslateUi(Properties)
        self.buttonBox.accepted.connect(Properties.accept)
        self.buttonBox.rejected.connect(Properties.reject)
        QtCore.QMetaObject.connectSlotsByName(Properties)

    def retranslateUi(self, Properties):
        _translate = QtCore.QCoreApplication.translate
        Properties.setWindowTitle(_translate("Properties", "Settings"))
        self.countryLabel.setToolTip(_translate("Properties", "Country code to insert in new application body"))
        self.countryLabel.setText(_translate("Properties", "Country"))
        self.dtdNameLabel.setText(_translate("Properties", "Dtd name"))
        self.dtdVersionLabel.setText(_translate("Properties", "Dtd version"))
        self.languageLabel.setToolTip(_translate("Properties", "Language of the new document. Used also in the conversion to PDF"))
        self.languageLabel.setText(_translate("Properties", "Language"))

